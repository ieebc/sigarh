from django.db import models

class TipoEmpleado(models.Model):
    descripcion = models.CharField(max_length=255)
    numero = models.IntegerField(default=0)

    def __str__(self):
        return self.descripcion

    def es_confianza(self):
        return (self.descripcion == "Confianza")

    def es_eventual(self):
        return (self.descripcion == "Eventual")

    def es_asimilable(self):
        return (self.descripcion == "Asimilable")