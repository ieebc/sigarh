from django.db import models

DEFAULT_PLAN_ID = 1
class Plan(models.Model):
    descripcion = models.CharField(max_length=255)
    clonado = models.BooleanField(default=False)
    eliminando = models.BooleanField(default=False)

    def __str__(self):
        return self.descripcion