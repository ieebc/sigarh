from django import forms
from nomina.models import *
from .class_select import class_form_select_sm

class SubsidioForm(forms.ModelForm):    
    class Meta:
        model = Subsidio
        fields = ['limite_inferior','limite_superior', 'cuota_fija']
        labels = {
            'limite_inferior': 'Límite Inferior',
            'limite_superior': 'Límite Superior',
            'cuota_fija': 'Cuota Fija',
        }

        widgets = {
            'limite_inferior': forms.NumberInput(attrs=class_form_select_sm),
            'limite_superior': forms.NumberInput(attrs=class_form_select_sm),
            'cuota_fija': forms.NumberInput(attrs=class_form_select_sm),
        }
