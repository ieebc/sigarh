(function() {
    const addPeriodoFormBtn = document.querySelector("#add-periodo-form");

    let periodoForm = document.getElementsByClassName("periodo-form");
    const mainForm = document.querySelector("#empleado-form");

    let totalForms = document.querySelector("#id_periodos-TOTAL_FORMS");

    let formCount = periodoForm.length - 1;

    function updateForms() {
        let count = 0;
        periodoForm = document.getElementsByClassName("periodo-form");
        for (let form of periodoForm) {
            const formRegex = RegExp(`form-(\\d){1}-`, 'g');
            if (count > 0) {
                let delete_button = form.getElementsByClassName("delete-periodo-form")[0]
                delete_button.style.display = 'block'
            }
            form.innerHTML = form.innerHTML.replace(formRegex, `form-${count++}-`)
        }
    }

    addPeriodoFormBtn.addEventListener("click", function (event) {
        event.preventDefault();
       
        // Clone a New Form
        const newPeriodoForm = periodoForm[0].cloneNode(true);

        const formRegex = RegExp(`form-(\\d){1}-`, 'g');
        formCount++;
        newPeriodoForm.innerHTML = newPeriodoForm.innerHTML.replace(formRegex, `form-${formCount}-`);
        addPeriodoFormBtn.parentNode.insertBefore(newPeriodoForm, addPeriodoFormBtn);
        updateForms();
        totalForms = document.querySelector("#id_periodos-TOTAL_FORMS");
        totalForms.setAttribute('value', `${formCount + 1}`);
    });

    mainForm.addEventListener("click", function (event) {
        if (event.target.classList.contains("delete-periodo-form")) {
            event.preventDefault();
            event.target.parentElement.parentElement.remove();
            formCount--;
            updateForms();
            totalForms = document.querySelector("#id_periodos-TOTAL_FORMS");
            totalForms.setAttribute('value', `${formCount + 1}`);

        }
    });

    
})();