from calendar import c
from time import strftime, strptime
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def fonacot_list(request):
    if request.user.is_authenticated:
        fonacots = Fonacot.objects.all()
        faltantes = 0
        for f in fonacots:
            #nombre_completo = f.nombre.split(" ")
            #paterno = nombre_completo[0]
            #materno = nombre_completo[1]
            #n = 2
            #nombre = ''

            #while n < len(nombre_completo):
            #    nombre += nombre_completo[n] + " "
            #    n += 1

            #f.nombre_trabajador = nombre[0:20]
            #f.apellido_paterno = paterno[0:20]
            #f.apellido_materno = materno[0:20]
            #f.save()

            if f.falta_informacion() > 0:
                faltantes += 1
        context = { 'fonacots': fonacots, 'faltantes': faltantes, 'url_name': 'fonacots'}
        return render(request, 'nomina/fonacot/content.html', context)
    else:
        return redirect('login')

@login_required
def fonacot_nuevo(request):
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = FonacotForm(request.POST)
        if form.is_valid():
            fonacot = form.save(commit=False)
            fonacot.user = current_user
            fonacot.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('fonacots')
    else:
        form = FonacotForm()
    return render(request, 'nomina/fonacot/modal-nuevo.html', {'form': form})

@login_required
def fonacot_editar(request, pk):
    fonacot = get_object_or_404(Fonacot, pk = pk)
    if request.method == 'POST':
        form = FonacotForm(request.POST, instance=fonacot)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de registro fonacot exitosa!')
            return redirect('fonacots')
    else:
        form = FonacotForm(instance=fonacot)
    return render(request, 'nomina/fonacot/modal-editar.html', {'form': form, 'fonacot': fonacot})

@login_required
def fonacot_eliminar(request, pk):
    fonacot = get_object_or_404(Fonacot, pk = pk)
    if request.is_ajax():
        context = { 'fonacot': fonacot }
        return render(request, 'nomina/fonacot/modal-eliminar.html', context)
    else:
        fonacot.delete()
        messages.success(request, 'Eliminación de registro fonacot exitosa!')
        return redirect('fonacots')

@login_required
def fonacot_exportar_txt(request):
    fonacots = Fonacot.objects.all()
    
    file_data = ''
    for f in fonacots:
        cadena = f.get_cadena()
        file_data += cadena + '\n'

    response = HttpResponse(file_data, content_type='application/text charset=ascii')
    response['Content-Disposition'] = 'attachment; filename="archivo_fonacot.txt"'
    return response

@login_required
def fonacot_exportar_csv(request):
    fonacots = Fonacot.objects.all()
    
    file_data = ''
    for f in fonacots:
        cadena = f.get_cadena()
        file_data += cadena + '\n'

    response = HttpResponse(file_data, content_type='application/text charset=ascii')
    response['Content-Disposition'] = 'attachment; filename="archivo_fonacot.csv"'
    return response