from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def subsidios_list(request):    
    plan = request.session['plan']
    subsidios = Subsidio.objects.filter(plan = plan)
    context = { 'subsidios': subsidios, 'url_name': 'subsidios'}
    return render(request, 'nomina/subsidios/content.html', context)

@login_required
def subsidio_nuevo(request):
    plan = request.session['plan']
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = SubsidioForm(request.POST)
        if form.is_valid():
            subsidio = form.save(commit=False)
            subsidio.user = current_user
            subsidio.plan = Plan.objects.filter(id = plan).first()
            subsidio.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('subsidios')
    else:
        form = SubsidioForm()
    return render(request, 'nomina/subsidios/modal-nuevo.html', {'form': form})

@login_required
def subsidio_editar(request, pk):
    subsidio = get_object_or_404(Subsidio, pk = pk)
    if request.method == 'POST':
        form = SubsidioForm(request.POST, instance=subsidio)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de registro Subsidio exitosa!')
            return redirect('subsidios')
    else:
        form = SubsidioForm(instance=subsidio)
    return render(request, 'nomina/subsidios/modal-editar.html', {'form': form, 'subsidio': subsidio})

@login_required
def subsidio_eliminar(request, pk):
    subsidio = get_object_or_404(Subsidio, pk = pk)
    if request.is_ajax():
        context = { 'subsidio': subsidio }
        return render(request, 'nomina/subsidios/modal-eliminar.html', context)
    else:
        subsidio.delete()
        messages.success(request, 'Eliminación de registro Subsidio exitosa!')
        return redirect('subsidios')