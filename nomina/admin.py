from django.contrib import admin
from nomina.models import *

# Register your models here.
admin.site.register(TipoEmpleado)
admin.site.register(Departamento)
admin.site.register(Puesto)
admin.site.register(Empleado)
admin.site.register(Partida)
admin.site.register(Rama)
admin.site.register(Nivel)
admin.site.register(Estructura)
admin.site.register(Periodo)
admin.site.register(Vacaciones)
admin.site.register(Plan)
admin.site.register(ISR)
admin.site.register(Subsidio)
admin.site.register(Fonacot)