from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def puestos_list(request):    
    if request.user.is_authenticated:
        plan = request.session['plan']
        puestos = Puesto.objects.filter(plan = plan)
        context = { 'puestos': puestos, 'url_name': 'puestos'}
        return render(request, 'nomina/puestos/content.html', context)
    else:
        return redirect('login')

@login_required
def puesto_nuevo(request):
    plan = request.session['plan']
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = PuestoForm(request.POST)
        if form.is_valid():
            puesto = form.save(commit=False)
            puesto.user = current_user
            puesto.orden = 1
            puesto.plan = Plan.objects.filter(id = plan).first()
            puesto.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('puestos')
    else:
        form = PuestoForm()
    return render(request, 'nomina/puestos/modal-nuevo.html', {'form': form})

@login_required
def puesto_editar(request, pk):
    puesto = get_object_or_404(Puesto, pk = pk)
    if request.method == 'POST':
        form = PuestoForm(request.POST, instance=puesto)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de puesto exitosa!')
            return redirect('puestos')
    else:
        form = PuestoForm(instance=puesto)
    return render(request, 'nomina/puestos/modal-editar.html', {'form': form, 'puesto': puesto})

@login_required
def puesto_eliminar(request, pk):
    plan = request.session['plan']
    puesto = get_object_or_404(Puesto, pk = pk)
    if request.is_ajax():
        context = { 'puesto': puesto }
        return render(request, 'nomina/puestos/modal-eliminar.html', context)
    else:
        Estructura.objects.filter(plan = plan, puesto = puesto).delete()
        Empleado.objects.filter(plan = plan, puesto = puesto).delete()
        puesto.delete()
        messages.success(request, 'Eliminación de puesto exitosa!')
        return redirect('puestos')