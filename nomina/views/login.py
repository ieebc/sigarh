from django.contrib.auth.views import LoginView  

from nomina.models import *
from nomina.forms import *

class SigaLoginView(LoginView):
    form_class = SigaLoginForm
    template_name='nomina/login.html'
    redirect_authenticated_user=True

    def form_valid(self, form):
        plan = form.cleaned_data['plan']
        self.request.session['plan'] = plan.id
        self.request.session['plan_descripcion'] = plan.descripcion
        return super().form_valid(form)