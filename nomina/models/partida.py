from django.db import models
from .plan import Plan, DEFAULT_PLAN_ID

class Partida(models.Model):
    descripcion = models.CharField(max_length=255)
    codigo = models.IntegerField()
    valor = models.DecimalField(max_digits=10, decimal_places=4, default=0)
    confianza = models.BooleanField(default=True)
    eventual = models.BooleanField(default=True)
    asimilable = models.BooleanField(default=True)
    incapacitado = models.BooleanField(default=True)
    orden = models.IntegerField(default=0)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return self.descripcion

    @property
    def descripcion_tipos(self):
        return self.descripcion + " " + self.tipos

    @property
    def tipos(self):
        tipos = []
        if self.confianza:
            tipos.append("Confianza")
        if self.eventual:
            tipos.append("Eventual")
        if self.asimilable:
            tipos.append("Asimilable")
        if self.incapacitado:
            tipos.append("Incapacitado")

        tipos_str = ''

        if len(tipos) == 1:
            tipos_str = '(%s)' % (tipos[0])

        if len(tipos) == 2:
            tipos_str = '(%s, %s)' % (tipos[0], tipos[1])

        if len(tipos) == 3:
            tipos_str = '(%s, %s, %s)' % (tipos[0], tipos[1], tipos[2])

        if len(tipos) == 4:
            tipos_str = '(%s, %s, %s, %s)' % (tipos[0], tipos[1], tipos[2], tipos[3])

        return tipos_str

    class Meta:
        ordering = ['orden']
