from django import forms
from nomina.models import *
from .class_select import class_form_select_sm

class PuestoForm(forms.ModelForm):    
    class Meta:
        model = Puesto
        fields = ['descripcion']
        labels = {
            'descripcion': 'Descripción',
        }

        widgets = {
            'descripcion': forms.TextInput(attrs=class_form_select_sm),
        }
