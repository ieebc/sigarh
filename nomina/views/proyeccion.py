import xlwt
import decimal
import datetime
from datetime import date
import time
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Q
from calendar import monthrange, isleap
from nomina.models import *
from nomina.forms import ProyeccionForm

class Proyeccion:
    def __init__(self, anio, tipo, departamento, puesto, nombre, detalle, plan):
        self.empleados = None
        self.partidas = None
        self.dias_anuales = 0
        self.total_plazas = 0
        self.total_proyeccion = 0

        self.data = {}
        self.acumulado_total = {}

        self.anio = anio
        self.tipo = tipo
        self.departamento = departamento
        self.puesto = puesto
        self.nombre = nombre
        self.detalle = int(detalle)
        self.plan = plan
        self.set_partidas()
        self.set_catorcenas()
        self.set_empleados()

        if isleap(self.anio):
            self.dias_anuales = 366
        else:
            self.dias_anuales = 365

        self.set_data()
        self.set_data_totales()

    def set_catorcenas(self):
        catorcenas = Catorcena.objects.filter(plan = self.plan, fecha_final__year = self.anio)
        self.catorcenas = catorcenas

    def set_partidas(self):
        if self.tipo.descripcion == "Confianza":        
            self.partidas = Partida.objects.filter(confianza = True, plan = self.plan)
        if self.tipo.descripcion == "Eventual":
            self.partidas = Partida.objects.filter(eventual = True, plan = self.plan)
        if self.tipo.descripcion == "Asimilable":
            self.partidas = Partida.objects.filter(asimilable = True, plan = self.plan)
        if self.tipo.descripcion == "Incapacitado":
            self.partidas = Partida.objects.filter(incapacitado = True, plan = self.plan)
        if self.anio not in (2018, 2021, 2024, 2027):
            self.partidas = self.partidas.exclude(Q(codigo = 13333) | Q(codigo = 13334))

    def set_empleados(self):
        if self.departamento == None and self.puesto == None:
            self.empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = self.tipo.descripcion), Q(nombre__icontains = self.nombre) | Q(apellido_paterno__icontains = self.nombre) | Q(apellido_materno__icontains = self.nombre), plan = self.plan)

        if self.departamento != None and self.puesto == None:
            self.empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = self.tipo.descripcion), Q(departamento__descripcion__contains = self.departamento.descripcion),  Q(nombre__icontains = self.nombre) | Q(apellido_paterno__icontains = self.nombre) | Q(apellido_materno__icontains = self.nombre), plan = self.plan)
            
        if self.departamento == None and self.puesto != None:
            self.empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = self.tipo.descripcion), Q(puesto__descripcion__contains = self.puesto.descripcion),  Q(nombre__icontains = self.nombre) | Q(apellido_paterno__icontains = self.nombre) | Q(apellido_materno__icontains = self.nombre), plan = self.plan)

        if self.departamento != None and self.puesto != None:
            self.empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = self.tipo.descripcion), Q(departamento__descripcion__contains = self.departamento.descripcion), Q(puesto__descripcion__contains = self.puesto.descripcion),  Q(nombre__icontains = self.nombre) | Q(apellido_paterno__icontains = self.nombre) | Q(apellido_materno__icontains = self.nombre), plan = self.plan)
        
        if self.tipo.descripcion == "Eventual":
            puestos = ['INCAPACITADO PERMANENTE "A"', 'INCAPACITADO PERMAMENTE "B"', 'INCAPACITADO PERMANENTE "C"']
            self.empleados = self.empleados.exclude(Q(puesto__descripcion__in = puestos))
        if self.tipo.descripcion == "Incapacitado":
            puestos = ['INCAPACITADO PERMANENTE "A"', 'INCAPACITADO PERMAMENTE "B"', 'INCAPACITADO PERMANENTE "C"']
            self.empleados = Empleado.objects.filter(Q(puesto__descripcion__in = puestos), plan = self.plan)

        self.empleados.order_by('departamento', 'puesto')

    def set_data_totales(self):
        if self.data['concentrado']['data']:
            self.total_plazas = self.data['concentrado']['data'][-1][0]
            self.total_proyeccion = self.data['concentrado']['data'][-1][-1]
    
    #PRINCIPAL
    def set_data(self):
        current_departamento = None
        acumulado_departamento = {}

        self.set_encabezados()

        for empleado in self.empleados:

            empleado.set_data_emp(self.anio, self.catorcenas)
            if (current_departamento is not None) and (empleado.departamento != current_departamento):
                self.concentrar_acumulado_departamento(acumulado_departamento, current_departamento.descripcion)
                acumulado_departamento = {}
            
            acumulado_departamento = self.calcular_acumular(empleado, acumulado_departamento)
            current_departamento = empleado.departamento

        if current_departamento is not None:
            self.concentrar_acumulado_departamento(acumulado_departamento, current_departamento.descripcion)
            acumulado_departamento = {}

        if self.acumulado_total:
            self.concentrar_acumulado_departamento(self.acumulado_total, 'Total')
            self.acumulado_total = {}
            
    #FUNCIONES DE ENCABEZADOS
    def set_encabezados(self):
        self.data['concentrado'] = {}
        self.data['concentrado']['encabezados'] = self.encabezados_partida('concentrado') 
        self.data['concentrado']['data'] = []

        for partida in self.partidas:
            self.data[partida.codigo] = {}
            self.data[partida.codigo]['encabezados'] = self.encabezados_partida(partida.codigo) 
            self.data[partida.codigo]['data'] = [] 

            #Antiguedad y Excedente no se concentran
            if partida.codigo != 13102 and partida.codigo != 13222: 
                self.data['concentrado']['encabezados'].append(partida.descripcion)

        self.data['concentrado']['encabezados'].append('Total')

    def encabezados_partida(self, codigo):
        if codigo == "concentrado":
            return self.template_encabezados_concentrado()
        
        if codigo == 13202:
            return self.template_encabezados_prima()
        else:
            return self.template_encabezados_default()

    def template_encabezados_default(self):
        template = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Fecha Ingreso', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY',
        'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC', 'Total']

        template_ev_as = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Periodo 1', 'Periodo 2', 'Periodo 3', 'Periodo 4', 'Periodo 5', 'Dias Totales','ENE', 'FEB', 'MAR', 'ABR', 'MAY',
        'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC', 'Total']

        if self.tipo.es_confianza():
            return template
        else:
            return template_ev_as
    
    def template_encabezados_prima(self):
        template = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Fecha Ingreso', 'Días Vacaciones', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY',
        'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC', 'Total']

        template_ev_as = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Periodo 1', 'Periodo 2', 'Periodo 3', 'Periodo 4', 'Periodo 5', 'Dias Totales', 'Dias Vacaciones', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY',
        'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC', 'Total']

        if self.tipo.es_confianza():
            return template
        else:
            return template_ev_as
        
    def template_encabezados_concentrado(self):
        template = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Fecha Ingreso']
        template_ev_as = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Periodo 1', 'Periodo 2', 'Periodo 3', 'Periodo 4', 'Periodo 5', 'Dias Totales']

        if self.tipo.es_confianza():
            return template
        else:
            return template_ev_as

    #FUNCIONES DE DATA
    def template_data_departamento(self, descripcion, plazas):
        data_departamento = []
        data_departamento.append(plazas)
        data_departamento.append(descripcion)
        data_departamento.append('')
        data_departamento.append('')
        data_departamento.append('')

        if not self.tipo.es_confianza():
            data_departamento.append('')
            data_departamento.append('')
            data_departamento.append('')
            data_departamento.append('')
            data_departamento.append('')

        return data_departamento

    def template_data_empleado(self, empleado):
        data_empleado = []
        data_empleado.append(empleado.plazas)
        data_empleado.append(empleado.puesto.descripcion)
        data_empleado.append(empleado.nombre_completo())
        data_empleado.append(empleado.sueldo)

        if self.tipo.es_confianza():
            data_empleado.append(empleado.fecha_ingreso)
        else:
            numero_periodos = 0
            dias_totales = 0
            for periodo in empleado.periodos:
                data_empleado.append(str(periodo))
                numero_periodos += 1
                dias_totales += periodo.dias

            for numero_periodos in range(numero_periodos, 5):
                data_empleado.append('')

            data_empleado.append(dias_totales)

        return data_empleado

    #FUNCIONES DE CONCENTRADO
    def concentrar_acumulado_departamento(self, acumulado_departamento, current_departamento):
        for partida in self.partidas:
            data_departamento_concentrado = self.concentrar_partida(acumulado_departamento, partida.codigo, current_departamento)
            if (self.detalle == 1 and current_departamento == 'Total') or (self.detalle > 1):
                self.data[partida.codigo]['data'].append(data_departamento_concentrado)
            if self.detalle > 2 and current_departamento != 'Total':
                self.data[partida.codigo]['data'] += acumulado_departamento[partida.codigo]

        data_departamento_concentrado = self.concentrar_partida(acumulado_departamento, 'concentrado', current_departamento)
        if (self.detalle == 1 and current_departamento == 'Total') or (self.detalle > 1):
            self.data['concentrado']['data'].append(data_departamento_concentrado)
        if self.detalle > 2 and current_departamento != 'Total':
            self.data['concentrado']['data'] += acumulado_departamento['concentrado']
    
    def concentrar_partida(self, acumulado_departamento, partida_codigo, descripcion):
        data_departamento_concentrado = []
        for data_empleado_dep in acumulado_departamento[partida_codigo]:
            if partida_codigo == 13202:
                if self.tipo.es_confianza():
                    data_departamento_concentrado.append(data_empleado_dep[6:])
                else:
                    data_departamento_concentrado.append(data_empleado_dep[11:])
            else:
                if self.tipo.es_confianza():
                    data_departamento_concentrado.append(data_empleado_dep[5:])
                else:
                    data_departamento_concentrado.append(data_empleado_dep[10:])
                    
        data_departamento_concentrado = [sum(i) for i in zip(*data_departamento_concentrado)]
        plazas = self.sumar_plazas(acumulado_departamento['concentrado'])
        if partida_codigo == 13202:
            data_departamento_concentrado = self.template_data_departamento(descripcion, plazas) + [''] + data_departamento_concentrado
        else:
            data_departamento_concentrado = self.template_data_departamento(descripcion, plazas) + data_departamento_concentrado

        #Se agrega al acumulado total
        if partida_codigo not in self.acumulado_total:
            self.acumulado_total[partida_codigo] = []
        self.acumulado_total[partida_codigo].append(data_departamento_concentrado)

        return data_departamento_concentrado

    def sumar_plazas(self, acumulado):
        result = sum(row[0] for row in acumulado)
        return result
        
    def calcular_acumular(self, empleado, acumulado_departamento):
        data_empleado_partida = []
        data_concentrado = []
        empleado_total = 0

        for partida in self.partidas:
            if partida.codigo == 13202:
                data_empleado_partida = self.template_data_empleado(empleado) + [str(empleado.dias_vacaciones)] + self.proyectar_partida(acumulado_departamento, partida, empleado)
            else:
                data_empleado_partida = self.template_data_empleado(empleado) + self.proyectar_partida(acumulado_departamento, partida, empleado)
            
            #Agregar partida al acumulado si no se hizo anteriormente
            if partida.codigo not in acumulado_departamento:
                acumulado_departamento[partida.codigo] = []
            acumulado_departamento[partida.codigo].append(data_empleado_partida)
                
            #Concentrado de empleado
            if partida.codigo != 13102 and partida.codigo != 13222:
                if partida.codigo != 10001:
                    empleado_total += data_empleado_partida[-1]
                data_concentrado.append(data_empleado_partida[-1])

            
        #Agregar concentrado
        data_concentrado = self.template_data_empleado(empleado) + data_concentrado
        data_concentrado.append(empleado_total)

        #Agregar concentrado al acumulado si no se hizo anteriormente
        if 'concentrado' not in acumulado_departamento:
            acumulado_departamento['concentrado'] = []
        acumulado_departamento['concentrado'].append(data_concentrado)
    
        return acumulado_departamento

    #FUNCIONES X PARTIDA PROYECCION
    def proyectar_partida(self, acumulado_departamento, partida, empleado):
        data_partida = []

        if partida.codigo == 11301 or partida.codigo == 13401:
            data_partida = self.proyectar_11301(partida.valor, empleado)
        if partida.codigo == 13202:
            data_partida = self.proyectar_13202(partida.valor, empleado)
        if partida.codigo == 13203:
            data_partida = self.proyectar_13203(partida.valor, empleado)
        if partida.codigo == 39801:
            data_partida = self.proyectar_39801(acumulado_departamento, partida.valor, empleado)
        if partida.codigo == 14101:
            data_partida = self.proyectar_14101(partida.valor, empleado)
        if partida.codigo == 14102:
            data_partida = self.proyectar_14102(acumulado_departamento, partida.valor, empleado)
        if partida.codigo == 14103:
            data_partida = self.proyectar_14103(acumulado_departamento, partida.valor, empleado)
        if partida.codigo == 13102:
            data_partida = self.proyectar_13102(partida.valor, empleado)
        if partida.codigo == 13222:
            data_partida = self.proyectar_13222(partida.valor, empleado)
        if partida.codigo == 13333:
            data_partida = self.proyectar_13333(partida.valor, empleado)
        if partida.codigo == 37804:
            data_partida = self.proyectar_37804(partida.valor, empleado)
        if partida.codigo == 15201:
            data_partida = self.proyectar_15201(partida.valor, empleado)
        if partida.codigo == 15402:
            data_partida = self.proyectar_15402(partida.valor, empleado)
        if partida.codigo == 10001:
            data_partida = self.proyectar_10001(partida.valor, empleado)
        if partida.codigo == 10002:
            data_partida = self.proyectar_10002(acumulado_departamento, partida.valor, empleado)

        return data_partida

    #SUELDO / COMPENSACION
    def proyectar_11301(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if i != 7 and i != 12:
                mes = empleado.plazas * valor * empleado.sueldo * 28
            if i == 7:
                mes = empleado.plazas * valor * empleado.sueldo * 42
            if i == 12:
                mes = empleado.plazas * valor * empleado.sueldo * 43
            
            #if empleado.tipo.es_confianza():
            #    mes = empleado.plazas * valor * empleado.sueldo * monthrange(self.anio,i)[1]
            #else:
            #    mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias[i - 1]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)    

        return data_partida

    #PRIMA
    def proyectar_13202(self, valor, empleado):
        data_partida = []
        total = 0
        
        for i in range(1,13):
            mes = 0
            if empleado.tipo.es_confianza():
                if i == 2 or i == 8:
                    mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / 2
        
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / self.dias_anuales * empleado.meses_dias_baja[i - 1]
            
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #VACACIONES
    def proyectar_15201(self, valor, empleado):
        data_partida = []
        total = 0

        for i in range(1,13):
            mes = 0
            if empleado.tipo.es_confianza():
                if i == 2 or i == 8:
                    mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / 2
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / self.dias_anuales * empleado.meses_dias_baja[i - 1]
            
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #AGUINALDO
    def proyectar_13203(self, valor, empleado):
        data_partida = []
        total = 0

        for i in range(1,13):
            mes = 0
            if empleado.tipo.es_confianza():
                if i == 12:
                    mes = empleado.plazas * valor * empleado.sueldo
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias_baja[i - 1] / self.dias_anuales

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #ISR 1.80
    def proyectar_39801(self, acumulado_departamento, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = valor * (acumulado_departamento[11301][-1][4 + i] + acumulado_departamento[13401][-1][4 + i] + acumulado_departamento[13202][-1][5 + i] + acumulado_departamento[13203][-1][4 + i])
            elif empleado.es_incapacitado():
                mes = valor * (acumulado_departamento[11301][-1][9 + i] + acumulado_departamento[13401][-1][9 + i] +  acumulado_departamento[13203][-1][9 + i])
            else:
                mes = valor * (acumulado_departamento[11301][-1][9 + i] + acumulado_departamento[13401][-1][9 + i] + acumulado_departamento[13202][-1][10 + i] + acumulado_departamento[13203][-1][9 + i])

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #ANTIGUEDAD
    def proyectar_13102(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = empleado.plazas * valor * empleado.sueldo
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #BONOS DE TRANSPORTE Y MOVILIDAD
    def proyectar_15402(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0
            if empleado.recibe_transporte:
                mes = empleado.plazas * valor * empleado.meses_dias_baja[i - 1]
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #GASTOS DE CAMPO
    def proyectar_37804(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0
            if empleado.recibe_gastos:
                if i == 5:
                    mes = empleado.plazas * valor
                if i == 6:
                    mes = empleado.plazas * valor * 4

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #ISSSTECALI
    def proyectar_14101(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = empleado.plazas * valor /14 * monthrange(self.anio,i)[1]
            else:
                mes = empleado.plazas * valor / 14 * empleado.meses_dias[i - 1]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    # ISSSTECALI
    def proyectar_14102(self, acumulado_departamento, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = empleado.plazas * valor * acumulado_departamento[11301][-1][4 + i]
            else:
                mes = empleado.plazas * valor * acumulado_departamento[11301][-1][9 + i]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)    

        return data_partida

    # ISSSTE
    def proyectar_14103(self, acumulado_departamento, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = empleado.plazas * valor * acumulado_departamento[11301][-1][4 + i]
            else:
                mes = empleado.plazas * valor * acumulado_departamento[11301][-1][9 + i]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)    

        return data_partida

    #EXCEDENTE
    def proyectar_13222(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0
            if i == 9:
                mes = empleado.plazas * valor * empleado.excedente * empleado.sueldo
            
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #BONO ELECTORAL
    def proyectar_13333(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0

            if i == 8 and empleado.recibe_bono:
                if empleado.tipo.es_confianza():
                    mes = empleado.plazas * valor * empleado.sueldo
                else:
                    mes = empleado.plazas * valor * empleado.sueldo * empleado.periodos_dias / self.dias_anuales

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #ISR
    def proyectar_10001(self, valor, empleado):
        data_partida = []
        meses = [0,0,0,0,0,0,0,0,0,0,0,0]
        total = 0
        no_catorcena = 0

        for catorcena in self.catorcenas:
            mes = catorcena.fecha_final.month
            dias_catorcena = empleado.catorcenas_dias[no_catorcena]
            percepcion_catorcenal = (empleado.sueldo * dias_catorcena) * valor
            retencion_catorcenal = self.get_retencion_catorcena(percepcion_catorcenal) - self.get_subsidio_catorcena(percepcion_catorcenal)
            meses[mes - 1] += retencion_catorcenal
            total = total + retencion_catorcenal
            no_catorcena += 1

        for mes in meses:
            data_partida.append(mes)
        
        data_partida.append(total)
        return data_partida

    #Bono Seguridad
    def proyectar_10002(self, acumulado_departamento, valor, empleado):
        data_partida = []
        meses = [0,0,0,0,0,0,0,0,0,0,0,0]
        total = 0
        no_catorcena = 0

        for catorcena in self.catorcenas:
            mes = catorcena.fecha_final.month
            dias_catorcena = empleado.catorcenas_dias[no_catorcena]
            percepcion_catorcenal = (empleado.sueldo * dias_catorcena) * valor
            retencion_catorcenal = self.get_retencion_catorcena(percepcion_catorcenal) - self.get_subsidio_catorcena(percepcion_catorcenal)
            meses[mes - 1] += retencion_catorcenal
            total = total + retencion_catorcenal
            no_catorcena += 1

        i = 1
        total = 0
        for mes in meses:
            mes_isr = 0
            if empleado.tipo.es_confianza():
                mes_isr = acumulado_departamento[10001][-1][4 + i]
            else:
                mes_isr = acumulado_departamento[10001][-1][9 + i]

            mes_bono = mes_isr - mes
            total += mes_bono
            data_partida.append(mes_bono)
            i += 1
        
        data_partida.append(total)
        return data_partida

    def get_retencion_catorcena(self, percepciones):
        registro_isr = ISR.objects.filter(plan = self.plan, limite_inferior__lte = percepciones).last()
        if registro_isr:
            excedente = percepciones - decimal.Decimal(registro_isr.limite_inferior)
            impuesto_marginal = excedente * decimal.Decimal(registro_isr.porcentaje)
            impuesto_sin_subsidio = impuesto_marginal + decimal.Decimal(registro_isr.cuota_fija)
            return impuesto_sin_subsidio
        return 0
    
    def get_subsidio_catorcena(self, percepciones):
        registro_subsidio = Subsidio.objects.filter(plan = self.plan, limite_inferior__lte = percepciones, limite_superior__gte = percepciones).last()
        if registro_subsidio:
            return decimal.Decimal(registro_subsidio.cuota_fija)
        return 0

    #REPORTE XLS
    def generar_excel(self):
        response = HttpResponse(content_type='application/ms-excel')
        departamento_nombre = "TODOS"
        if self.departamento:
            departamento_nombre = self.departamento.descripcion

        response['Content-Disposition'] = 'attachment; filename="PROYECCION_NOMINA_%s_%s.xls"' % (self.tipo.descripcion, departamento_nombre)
        libro = xlwt.Workbook(encoding='utf-8', style_compression=2)

        #CONCENTRADO
        libro = self.generar_hoja_partida('concentrado', 'Concentrado', libro)

        #PARTIDAS
        for partida in self.partidas:
            libro = self.generar_hoja_partida(partida.codigo, partida.descripcion, libro)

        libro.save(response)
        return response

    def generar_hoja_partida(self, partida_codigo, partida_descripcion, libro):
        #Declarar estilos
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = xlwt.Style.colour_map['periwinkle']

        borders = xlwt.Borders()
        borders.left = xlwt.Borders.THIN
        borders.right = xlwt.Borders.THIN
        borders.top = xlwt.Borders.THIN
        borders.bottom = xlwt.Borders.THIN

        font_style_default = xlwt.XFStyle()
        font_style_default.borders = borders

        font_style_titulo_hoja = xlwt.XFStyle()
        font_style_titulo_hoja.font.bold = True
        font_style_titulo_hoja.font.height = 320

        font_style_encabezados = xlwt.XFStyle()
        font_style_encabezados.font.bold = True
        font_style_encabezados.pattern = pattern
        font_style_encabezados.borders = borders



        hoja = libro.add_sheet(partida_descripcion)      

        col_area = hoja.col(1)  
        col_area.width = 256 * 60

        col_nombre = hoja.col(2)  
        col_nombre.width = 256 * 40
        
        hoja.write(0, 0, partida_descripcion, font_style_titulo_hoja)

        row_num = 2
        proyeccion_partida = self.data[partida_codigo]        
        for col_num in range(len(proyeccion_partida['encabezados'])):
            hoja.write(row_num, col_num, proyeccion_partida['encabezados'][col_num], font_style_encabezados)

        row_num += 1

        font_style = xlwt.XFStyle()
        font_style_default = xlwt.XFStyle()

        for empleado in proyeccion_partida['data']:
            
            #Si es encabezado
            if empleado[2] == '':
                font_style = font_style_encabezados
            else:
                font_style = font_style_default

            for col_num in range(len(empleado)):
                if self.es_moneda(empleado[col_num]):
                    font_style.num_format_str = '$#,##0.00'
                elif self.es_fecha(empleado[col_num]):
                    font_style.num_format_str = 'dd/mm/yyyy'
                else:
                    font_style.num_format_str = 'General'

                hoja.write(row_num, col_num, empleado[col_num], font_style) 
            
            row_num += 1

        return libro

    #FORMATO DE PROYECCION
    def es_moneda(self, value):
        return (isinstance(value, decimal.Decimal))

    def es_fecha(self, value):
        return (isinstance(value, datetime.date))

@login_required
def proyeccion_list(request):
    anio = date.today().year + 1
    tipo = TipoEmpleado.objects.filter(descripcion = "Confianza").first()
    departamento = None
    puesto = None
    nombre = ""
    detalle = 1

    plan = request.session['plan']

    if request.method == 'POST':
        form = ProyeccionForm(plan, request.POST)
        if form.is_valid():
            anio = int(form.cleaned_data['anio'])
            tipo = form.cleaned_data['tipo']
            departamento = form.cleaned_data['departamento']
            puesto = form.cleaned_data['puesto']
            nombre = form.cleaned_data['nombre']
            detalle = form.cleaned_data['detalle']
    else:
        form = ProyeccionForm(plan)

    if 'exportar' in request.POST:
        detalle = 3
        proyeccion = Proyeccion(anio, tipo, departamento, puesto, nombre, detalle, plan)
        response = proyeccion.generar_excel()
        return response

    proyeccion = Proyeccion(anio, tipo, departamento, puesto, nombre, detalle, plan)
    return render(request, 'nomina/menu/proyeccion.html', { 'form': form, 'url_name': 'proyeccion', 'proyeccion': proyeccion})