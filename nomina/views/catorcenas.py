import datetime
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def catorcenas_list(request):   
    plan = request.session['plan'] 
    if request.method == 'POST':
        form = CatorcenasSyncForm(request.POST)
    else:
        form = CatorcenasSyncForm()

    catorcenas = Catorcena.objects.filter(plan = plan)
    context = { 'catorcenas': catorcenas, 'url_name': 'catorcenas', 'form': form}
    return render(request, 'nomina/catorcenas/content.html', context)

@login_required
def catorcena_sincronizar(request):
    plan = request.session['plan']
    if request.method == 'POST':
        form = CatorcenasSyncForm(request.POST)
        if form.is_valid():
            fecha_inicial = form.cleaned_data['fecha_inicial']
            Catorcena.objects.filter(plan = plan).delete()
            fecha_limite = fecha_inicial.replace(month=12, day=31)
            periodo = 1
            anio = fecha_inicial.year
            while fecha_inicial <= fecha_limite:
                fecha_final = fecha_inicial + datetime.timedelta(days=13)

                catorcena = Catorcena()
                catorcena.fecha_inicio = fecha_inicial
                catorcena.fecha_final = fecha_final
                catorcena.descripcion = 'Periodo Catorcenal %s' % (periodo)
                catorcena.plan = Plan.objects.filter(id = plan).first()
                if fecha_final.year <= anio:
                    catorcena.save()
                
                fecha_inicial = fecha_final + datetime.timedelta(days=1)
                periodo += 1

            messages.success(request, 'Catorcenas Sincronizadas!')

    return redirect('catorcenas')

