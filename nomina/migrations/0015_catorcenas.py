# Generated by Django 3.2.5 on 2021-10-27 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nomina', '0014_auto_20211026_1414'),
    ]

    operations = [
        migrations.CreateModel(
            name='Catorcenas',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField()),
            ],
            options={
                'ordering': ['fecha_inicio'],
            },
        ),
    ]
