from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import DepartamentoForm

@login_required
def departamentos_list(request): 
    plan = request.session['plan']  
    departamentos = Departamento.objects.filter(plan = plan).order_by('orden')
    orden = 1
    for departamento in departamentos:
        departamento.orden = orden
        departamento.save()
        orden += 1
            
    departamentos = Departamento.objects.filter(plan = plan).order_by('orden')
    context = { 'departamentos': departamentos, 'url_name': 'departamentos'}
    return render(request, 'nomina/departamentos/content.html', context)

@login_required
def departamento_nuevo(request):
    plan = request.session['plan']
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = DepartamentoForm(request.POST)
        if form.is_valid():
            departamento = form.save(commit=False)
            departamento.user = current_user
            departamento.orden = Departamento.objects.filter(plan = plan).count() + 1
            departamento.plan = Plan.objects.filter(id = plan).first()
            departamento.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('departamentos')
    else:
        form = DepartamentoForm()
    return render(request, 'nomina/departamentos/modal-nuevo.html', {'form': form})

@login_required
def departamento_editar(request, pk):
    departamento = get_object_or_404(Departamento, pk = pk)
    if request.method == 'POST':
        form = DepartamentoForm(request.POST, instance=departamento)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de departamento exitosa!')
            return redirect('departamentos')
    else:
        form = DepartamentoForm(instance=departamento)
    return render(request, 'nomina/departamentos/modal-editar.html', {'form': form, 'departamento': departamento})

@login_required
def departamento_eliminar(request, pk):
    plan = request.session['plan']
    departamento = get_object_or_404(Departamento, pk = pk)
    if request.is_ajax():
        context = { 'departamento': departamento }
        return render(request, 'nomina/departamentos/modal-eliminar.html', context)
    else:
        Empleado.objects.filter(plan = plan, departamento = departamento).delete()
        departamento.delete()
        messages.success(request, 'Eliminación de departamento exitosa!')
        return redirect('departamentos')

def departamento_subir(request, pk):
    plan = request.session['plan']
    departamento_sube = get_object_or_404(Departamento, pk = pk)
    departamento_baja = Departamento.objects.filter(orden = departamento_sube.orden - 1, plan = plan).first()
    if departamento_baja:
        departamento_sube.orden -= 1
        departamento_baja.orden += 1

        departamento_sube.save()
        departamento_baja.save()

    return redirect('departamentos')

def departamento_bajar(request, pk):
    plan = request.session['plan']
    departamento_baja = get_object_or_404(Departamento, pk = pk)
    departamento_sube = Departamento.objects.filter(orden = departamento_baja.orden + 1, plan = plan).first()

    if departamento_sube:
        departamento_sube.orden -= 1
        departamento_baja.orden += 1

        departamento_sube.save()
        departamento_baja.save()
        
    return redirect('departamentos')