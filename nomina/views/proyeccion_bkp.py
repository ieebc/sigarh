import xlwt
import decimal
import datetime
from datetime import date
import time
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Q
from calendar import monthrange, isleap
from nomina.models import *
from nomina.forms import ProyeccionForm


PUESTOS_INCAPACITADOS = ['INCAPACITADO PERMANENTE "A"', 'INCAPACITADO PERMAMENTE "B"', 'INCAPACITADO PERMANENTE "C"']

class Proyeccion:

    def __init__(self, anio, tipo, departamento, puesto, nombre, detalle, plan):
        self.total_plazas = 0
        self.total_proyeccion = 0

        self.data = {}
        self.acumulado_total = {}

        self.anio = anio
        self.tipo = tipo
        self.detalle = int(detalle)

        self.dias_anuales = self.get_dias_anuales(anio)
        self.catorcenas = self.get_catorcenas(plan, anio)
        self.isrs = self.get_registros_isr(plan)
        self.subsidios = self.get_registros_subsidio(plan)
        self.partidas = self.get_partidas(plan, anio, tipo)
        self.empleados = self.get_empleados(plan, tipo, departamento, puesto, nombre)
        
        self.set_data()
        self.set_data_totales(self.data)


    #   #   #   #   #   #   #   #   #   #
    #   #   INICIALIZAR PROYECCION  #   # 
    #   #   #   #   #   #   #   #   #   #

    def get_dias_anuales(self, anio):
        if isleap(anio):
            return 366
        return 365

    def get_catorcenas(self, plan, anio):
        return Catorcena.objects.filter(plan = plan, fecha_final__year = anio)

    def get_registros_isr(self, plan):
        return ISR.objects.filter(plan = plan)

    def get_registros_subsidio(self, plan):
        return Subsidio.objects.filter(plan = plan)

    def get_partidas(self, plan, anio, tipo):
        if tipo.descripcion == "Confianza":        
            partidas = Partida.objects.filter(confianza = True, plan = plan)
        if tipo.descripcion == "Eventual":
            partidas = Partida.objects.filter(eventual = True, plan = plan)
        if tipo.descripcion == "Asimilable":
            partidas = Partida.objects.filter(asimilable = True, plan = plan)
        if tipo.descripcion == "Incapacitado":
            partidas = Partida.objects.filter(incapacitado = True, plan = plan)

        if anio not in (2018, 2021, 2024, 2027):
            partidas = partidas.exclude(Q(codigo = 13333) | Q(codigo = 13334))

        return partidas

    def get_empleados(self, plan, tipo, departamento, puesto, nombre):
        if departamento == None and puesto == None:
            empleados = Empleado.objects.filter(Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), tipo = tipo, plan = plan)
        if departamento != None and puesto == None:
            empleados = Empleado.objects.filter(Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), tipo = tipo, departamento = departamento, plan = plan)
        if departamento == None and puesto != None:
            empleados = Empleado.objects.filter(Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), tipo = tipo, puesto = puesto, plan = plan)
        if departamento != None and puesto != None:
            empleados = Empleado.objects.filter(Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), tipo = tipo, departamento = departamento, puesto = puesto, plan = plan)
        
        if tipo.descripcion == "Incapacitado":
            empleados = empleados.filter(Q(puesto__descripcion__in = PUESTOS_INCAPACITADOS))
        else:
            empleados = empleados.exclude(Q(puesto__descripcion__in = PUESTOS_INCAPACITADOS))
        
        empleados.order_by('departamento', 'puesto')
        return empleados
    
    
    #   #   #   #   #   #   #   #   #   #   #
    #   #   #   GENERAR PROYECCION  #   #   # 
    #   #   #   #   #   #   #   #   #   #   #

    def set_data(self):
        current_departamento = None
        acumulado_departamento = {}

        self.set_encabezados()

        for empleado in self.empleados:
            empleado.set_data_emp(self.anio, self.catorcenas)
            if (current_departamento is not None) and (empleado.departamento != current_departamento):
                self.concentrar_acumulado_departamento(acumulado_departamento, current_departamento.descripcion)
                acumulado_departamento = {}
            
            acumulado_departamento = self.calcular_acumular(empleado, acumulado_departamento)
            current_departamento = empleado.departamento

        if current_departamento is not None:
            self.concentrar_acumulado_departamento(acumulado_departamento, current_departamento.descripcion)
            acumulado_departamento = {}

        if self.acumulado_total:
            self.concentrar_acumulado_departamento(self.acumulado_total, 'Total')
            self.acumulado_total = {}
            
    #FUNCIONES DE ENCABEZADOS
    def set_encabezados(self):
        self.data['concentrado'] = {}
        self.data['concentrado']['encabezados'] = self.encabezado_partida('concentrado') 
        self.data['concentrado']['data'] = []

        for partida in self.partidas:
            self.data[partida.codigo] = {}
            self.data[partida.codigo]['encabezados'] = self.encabezado_partida(partida.codigo) 
            self.data[partida.codigo]['data'] = [] 

            #Excluir partidas de ser mostradas en el Concentrado (Antiguedad y Excedente)
            if partida.codigo not in [13102, 13222]:
                self.data['concentrado']['encabezados'].append(partida.descripcion)

        self.data['concentrado']['encabezados'].append('Total')

    def encabezado_partida(self, codigo_partida):
        template_default = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Fecha Ingreso']
        template_ev_as = ['Plazas', 'Area / Puesto', 'Nombre', 'Sueldo Diario', 'Periodo 1', 'Periodo 2', 'Periodo 3', 'Periodo 4', 'Periodo 5', 'Dias Totales']

        template_meses = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC', 'Total']

        if self.tipo.es_confianza():
            template = template_default
        else:
            template = template_ev_as

        if codigo_partida != "concentrado":
            template += template_meses
        
        return template

    #FUNCIONES DE DATA
    def template_data_departamento(self, plazas, descripcion):
        data_departamento = []
        data_departamento.append(plazas)
        data_departamento.append(descripcion)

        vacios = 3
        if not self.tipo.es_confianza():
            vacios = 8

        for _ in range(vacios):
            data_departamento.append('')

        return data_departamento

    def template_data_empleado(self, empleado):
        data_empleado = []
        data_empleado.append(empleado.plazas)
        data_empleado.append(empleado.puesto.descripcion)
        data_empleado.append(empleado.nombre_completo())
        data_empleado.append(empleado.sueldo)

        if self.tipo.es_confianza():
            data_empleado.append(empleado.fecha_ingreso)
        else:
            numero_periodos = 0
            dias_totales = 0
            for periodo in empleado.periodos:
                data_empleado.append(str(periodo))
                numero_periodos += 1
                dias_totales += periodo.dias

            for numero_periodos in range(numero_periodos, 5):
                data_empleado.append('')

            data_empleado.append(dias_totales)

        return data_empleado

    #FUNCIONES DE CONCENTRADO
    def concentrar_acumulado_departamento(self, acumulado_departamento, current_departamento):
        for partida in self.partidas:
            data_departamento_concentrado = self.concentrar_partida(acumulado_departamento, partida.codigo, current_departamento)
            if (self.detalle == 1 and current_departamento == 'Total') or (self.detalle > 1):
                self.data[partida.codigo]['data'].append(data_departamento_concentrado)
            if self.detalle > 2 and current_departamento != 'Total':
                self.data[partida.codigo]['data'] += acumulado_departamento[partida.codigo]

        data_departamento_concentrado = self.concentrar_partida(acumulado_departamento, 'concentrado', current_departamento)
        if (self.detalle == 1 and current_departamento == 'Total') or (self.detalle > 1):
            self.data['concentrado']['data'].append(data_departamento_concentrado)
        if self.detalle > 2 and current_departamento != 'Total':
            self.data['concentrado']['data'] += acumulado_departamento['concentrado']
    
    def concentrar_partida(self, acumulado_departamento, partida_codigo, descripcion):
        data_departamento_concentrado = []
        for data_empleado_dep in acumulado_departamento[partida_codigo]:
            if self.tipo.es_confianza():
                data_departamento_concentrado.append(data_empleado_dep[5:])
            else:
                data_departamento_concentrado.append(data_empleado_dep[10:])
                    
        data_departamento_concentrado = [sum(i) for i in zip(*data_departamento_concentrado)]
        plazas = self.sumar_plazas(acumulado_departamento['concentrado'])
        data_departamento_concentrado = self.template_data_departamento(plazas, descripcion) + data_departamento_concentrado

        #Se agrega al acumulado total
        if partida_codigo not in self.acumulado_total:
            self.acumulado_total[partida_codigo] = []
        self.acumulado_total[partida_codigo].append(data_departamento_concentrado)

        return data_departamento_concentrado

    def sumar_plazas(self, acumulado):
        result = sum(row[0] for row in acumulado)
        return result
        
    def calcular_acumular(self, empleado, acumulado_departamento):
        data_empleado_partida = []
        data_concentrado = []
        empleado_total = 0

        for partida in self.partidas:
            data_empleado_partida = self.template_data_empleado(empleado) + self.proyectar_partida(acumulado_departamento, partida, empleado)

            #Agregar partida al acumulado si no se hizo anteriormente
            if partida.codigo not in acumulado_departamento:
                acumulado_departamento[partida.codigo] = []
            acumulado_departamento[partida.codigo].append(data_empleado_partida)
                
            #Concentrado de empleado
            if partida.codigo != 13102 and partida.codigo != 13222:
                if partida.codigo != 10001:
                    empleado_total += data_empleado_partida[-1]
                data_concentrado.append(data_empleado_partida[-1])

            
        #Agregar concentrado
        data_concentrado = self.template_data_empleado(empleado) + data_concentrado
        data_concentrado.append(empleado_total)

        #Agregar concentrado al acumulado si no se hizo anteriormente
        if 'concentrado' not in acumulado_departamento:
            acumulado_departamento['concentrado'] = []
        acumulado_departamento['concentrado'].append(data_concentrado)
    
        return acumulado_departamento
    
    def set_data_totales(self, data):
        if data['concentrado']['data']:
            self.total_plazas = data['concentrado']['data'][-1][0]
            self.total_proyeccion = data['concentrado']['data'][-1][-1]

    #FUNCIONES X PARTIDA PROYECCION
    def proyectar_partida(self, acumulado_departamento, partida, empleado):
        data_partida = []

        if partida.codigo == 11301 or partida.codigo == 13401:
            data_partida = self.proyectar_11301(partida.valor, empleado)
        if partida.codigo == 13202:
            data_partida = self.proyectar_13202(partida.valor, empleado)
        if partida.codigo == 13203:
            data_partida = self.proyectar_13203(partida.valor, empleado)
        if partida.codigo == 39801:
            data_partida = self.proyectar_39801(acumulado_departamento, partida.valor, empleado)
        if partida.codigo == 14101:
            data_partida = self.proyectar_14101(partida.valor, empleado)
        if partida.codigo == 14102:
            data_partida = self.proyectar_14102(acumulado_departamento, partida.valor, empleado)
        if partida.codigo == 13102:
            data_partida = self.proyectar_13102(partida.valor, empleado)
        if partida.codigo == 13222:
            data_partida = self.proyectar_13222(partida.valor, empleado)
        if partida.codigo == 13333:
            data_partida = self.proyectar_13333(partida.valor, empleado)
        if partida.codigo == 37804:
            data_partida = self.proyectar_37804(partida.valor, empleado)
        if partida.codigo == 15201:
            data_partida = self.proyectar_15201(partida.valor, empleado)
        if partida.codigo == 15402:
            data_partida = self.proyectar_15402(partida.valor, empleado)
        if partida.codigo == 10001:
            data_partida = self.proyectar_10001(partida.valor, empleado)

        return data_partida

    def proyectar_general(self, partida, empleado, acumulado_departamento):
        data_partida = []
        total = 0
        for no_mes in range(1,13):
            mes = self.calcular_partida_mes(no_mes, partida, empleado, acumulado_departamento)
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    def calcular_partida_mes(self, no_mes, partida, empleado, acumulado_departamento):
        if partida.codigo in [11301, 13401]:
            return self.calcular_11301_13401(no_mes, partida.valor, empleado)
        if partida.codigo == 13202:
            return self.calcular_13202(no_mes, partida.valor, empleado)
        if partida.codigo == 15201:
            return self.calcular_15201(no_mes, partida.valor, empleado)
        if partida.codigo == 13203:
            return self.calcular_13203(no_mes, partida.valor, empleado) 

    #SUELDO / COMPENSACION
    def calcular_11301_13401(self, no_mes, valor, empleado):
        mes = 0
        if empleado.tipo.es_confianza():
            mes = empleado.plazas * valor * empleado.sueldo * monthrange(self.anio, no_mes)[1]
        else:
            mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias[no_mes - 1]
        return mes

    #PRIMA
    def calcular_13202(self, no_mes, valor, empleado):
        mes = 0
        if not empleado.tipo.es_confianza():
            mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones/ self.dias_anuales * empleado.meses_dias_baja[no_mes - 1]
        else:
            if no_mes in [2, 8]:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / 2

        return mes

    #VACACIONES
    def calcular_15201(self, no_mes, valor, empleado):
        mes = 0
        if not empleado.tipo.es_confianza():
            mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones/ self.dias_anuales * empleado.meses_dias_baja[no_mes - 1]
        else:
            if no_mes in [2, 8]:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / 2

        return mes

    #AGUINALDO
    def calcular_13203(self, no_mes, valor, empleado):
        mes = 0
        if not empleado.tipo.es_confianza():
            mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias_baja[no_mes - 1] / self.dias_anuales
        else:
            if no_mes == 12:
                mes = empleado.plazas * valor * empleado.sueldo

        return mes

    #1.80 ESTADO
    def calcular_39801(self, no_mes, valor, empleado):
        mes = 0
        if not empleado.tipo.es_confianza():
            mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias_baja[no_mes - 1] / self.dias_anuales
        else:
            if no_mes == 12:
                mes = empleado.plazas * valor * empleado.sueldo

        return mes

    #SUELDO / COMPENSACION
    def proyectar_11301(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = empleado.plazas * valor * empleado.sueldo * monthrange(self.anio,i)[1]
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias[i - 1]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)    

        return data_partida

    #PRIMA
    def proyectar_13202(self, valor, empleado):
        data_partida = []
        total = 0

        for i in range(1,13):
            mes = 0
            if empleado.tipo.es_confianza():
                if i == 2 or i == 8:
                    mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / 2
        
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones/ self.dias_anuales * empleado.meses_dias_baja[i - 1]
            
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #VACACIONES
    def proyectar_15201(self, valor, empleado):
        data_partida = []
        total = 0

        for i in range(1,13):
            mes = 0
            if empleado.tipo.es_confianza():
                if i == 2 or i == 8:
                    mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / 2
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.dias_vacaciones / self.dias_anuales * empleado.meses_dias_baja[i - 1]
            
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #AGUINALDO
    def proyectar_13203(self, valor, empleado):
        data_partida = []
        total = 0

        for i in range(1,13):
            mes = 0
            if empleado.tipo.es_confianza():
                if i == 12:
                    mes = empleado.plazas * valor * empleado.sueldo
            else:
                mes = empleado.plazas * valor * empleado.sueldo * empleado.meses_dias_baja[i - 1] / self.dias_anuales

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #ISR 1.80
    def proyectar_39801(self, acumulado_departamento, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = valor * (acumulado_departamento[11301][-1][4 + i] + acumulado_departamento[13401][-1][4 + i] + acumulado_departamento[13202][-1][4 + i] + acumulado_departamento[13203][-1][4 + i])
            elif empleado.es_incapacitado():
                mes = valor * (acumulado_departamento[11301][-1][9 + i] + acumulado_departamento[13401][-1][9 + i] + acumulado_departamento[13203][-1][9 + i])
            else:
                mes = valor * (acumulado_departamento[11301][-1][9 + i] + acumulado_departamento[13401][-1][9 + i] + acumulado_departamento[13202][-1][9 + i] + acumulado_departamento[13203][-1][9 + i])

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #ANTIGUEDAD
    def proyectar_13102(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = empleado.plazas * valor * empleado.sueldo
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #BONOS DE TRANSPORTE Y MOVILIDAD
    def proyectar_15402(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0
            if empleado.recibe_transporte:
                mes = empleado.plazas * valor * empleado.meses_dias_baja[i - 1]
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #GASTOS DE CAMPO
    def proyectar_37804(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0
            if empleado.recibe_gastos:
                if i == 5:
                    mes = empleado.plazas * valor
                if i == 6:
                    mes = empleado.plazas * valor * 4

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)
        return data_partida

    #ISSSTECALI
    def proyectar_14101(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = empleado.plazas * valor /14 * monthrange(self.anio,i)[1]
            else:
                mes = empleado.plazas * valor / 14 * empleado.meses_dias[i - 1]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    # ISSSTE
    def proyectar_14102(self, acumulado_departamento, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            if empleado.tipo.es_confianza():
                mes = empleado.plazas * valor * acumulado_departamento[11301][-1][4 + i]
            else:
                mes = empleado.plazas * valor * acumulado_departamento[11301][-1][9 + i]

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)    

        return data_partida

    #EXCEDENTE
    def proyectar_13222(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0
            if i == 9:
                mes = empleado.plazas * valor * empleado.excedente * empleado.sueldo
            
            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #BONO ELECTORAL
    def proyectar_13333(self, valor, empleado):
        data_partida = []
        total = 0
        for i in range(1,13):
            mes = 0

            if i == 8 and empleado.recibe_bono:
                if empleado.tipo.es_confianza():
                    mes = empleado.plazas * valor * empleado.sueldo
                else:
                    mes = empleado.plazas * valor * empleado.sueldo * empleado.periodos_dias / self.dias_anuales

            total = total + mes
            data_partida.append(mes)

        data_partida.append(total)

        return data_partida

    #ISR
    def proyectar_10001(self, valor, empleado):
        data_partida = []
        meses = [0,0,0,0,0,0,0,0,0,0,0,0]
        total = 0
        no_catorcena = 0

        for catorcena in self.catorcenas:
            mes = catorcena.fecha_final.month
            dias_catorcena = empleado.catorcenas_dias[no_catorcena]
            percepcion_catorcenal = (empleado.sueldo * dias_catorcena) * valor
            retencion_catorcenal = self.get_retencion_catorcena(percepcion_catorcenal) - self.get_subsidio_catorcena(percepcion_catorcenal)
            meses[mes - 1] += retencion_catorcenal
            total = total + retencion_catorcenal
            no_catorcena += 1

        for mes in meses:
            data_partida.append(mes)
        
        data_partida.append(total)
        return data_partida


    def get_retencion_catorcena(self, percepciones):
        registro_isr = self.isrs.filter(limite_inferior__lte = percepciones).last()
        if registro_isr:
            excedente = percepciones - decimal.Decimal(registro_isr.limite_inferior)
            impuesto_marginal = excedente * decimal.Decimal(registro_isr.porcentaje)
            impuesto_sin_subsidio = impuesto_marginal + decimal.Decimal(registro_isr.cuota_fija)
            return impuesto_sin_subsidio
        return 0
    
    def get_subsidio_catorcena(self, percepciones):
        registro_subsidio = self.subsidios.filter(limite_inferior__lte = percepciones, limite_superior__gte = percepciones).last()
        if registro_subsidio:
            return decimal.Decimal(registro_subsidio.cuota_fija)
        return 0

    #REPORTE XLS
    def generar_excel(self, departamento):
        response = HttpResponse(content_type='application/ms-excel')
        departamento_nombre = "TODOS"
        if departamento:
            departamento_nombre = departamento.descripcion

        response['Content-Disposition'] = 'attachment; filename="PROYECCION_NOMINA_%s_%s.xls"' % (self.tipo.descripcion, departamento_nombre)
        libro = xlwt.Workbook(encoding='utf-8', style_compression=2)

        #CONCENTRADO
        libro = self.generar_hoja_partida('concentrado', 'Concentrado', libro)

        #PARTIDAS
        for partida in self.partidas:
            libro = self.generar_hoja_partida(partida.codigo, partida.descripcion, libro)

        libro.save(response)
        return response

    def generar_hoja_partida(self, partida_codigo, partida_descripcion, libro):
        #Declarar estilos
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = xlwt.Style.colour_map['periwinkle']

        borders = xlwt.Borders()
        borders.left = xlwt.Borders.THIN
        borders.right = xlwt.Borders.THIN
        borders.top = xlwt.Borders.THIN
        borders.bottom = xlwt.Borders.THIN

        font_style_default = xlwt.XFStyle()
        font_style_default.borders = borders

        font_style_titulo_hoja = xlwt.XFStyle()
        font_style_titulo_hoja.font.bold = True
        font_style_titulo_hoja.font.height = 320

        font_style_encabezados = xlwt.XFStyle()
        font_style_encabezados.font.bold = True
        font_style_encabezados.pattern = pattern
        font_style_encabezados.borders = borders



        hoja = libro.add_sheet(partida_descripcion)      

        col_area = hoja.col(1)  
        col_area.width = 256 * 60

        col_nombre = hoja.col(2)  
        col_nombre.width = 256 * 40
        
        hoja.write(0, 0, partida_descripcion, font_style_titulo_hoja)

        row_num = 2
        proyeccion_partida = self.data[partida_codigo]        
        for col_num in range(len(proyeccion_partida['encabezados'])):
            hoja.write(row_num, col_num, proyeccion_partida['encabezados'][col_num], font_style_encabezados)

        row_num += 1

        font_style = xlwt.XFStyle()
        font_style_default = xlwt.XFStyle()

        for empleado in proyeccion_partida['data']:
            
            #Si es encabezado
            if empleado[2] == '':
                font_style = font_style_encabezados
            else:
                font_style = font_style_default

            for col_num in range(len(empleado)):
                if self.es_moneda(empleado[col_num]):
                    font_style.num_format_str = '$#,##0.00'
                elif self.es_fecha(empleado[col_num]):
                    font_style.num_format_str = 'dd/mm/yyyy'
                else:
                    font_style.num_format_str = 'General'

                hoja.write(row_num, col_num, empleado[col_num], font_style) 
            
            row_num += 1

        return libro

    #FORMATO DE PROYECCION
    def es_moneda(self, value):
        return (isinstance(value, decimal.Decimal))

    def es_fecha(self, value):
        return (isinstance(value, datetime.date))

@login_required
def proyeccion_list(request):
    anio = date.today().year + 1
    tipo = TipoEmpleado.objects.filter(descripcion = "Confianza").first()
    departamento = None
    puesto = None
    nombre = ""
    detalle = 1

    plan = request.session['plan']

    if request.method == 'POST':
        form = ProyeccionForm(plan, request.POST)
        if form.is_valid():
            anio = int(form.cleaned_data['anio'])
            tipo = form.cleaned_data['tipo']
            departamento = form.cleaned_data['departamento']
            puesto = form.cleaned_data['puesto']
            nombre = form.cleaned_data['nombre']
            detalle = form.cleaned_data['detalle']
    else:
        form = ProyeccionForm(plan)

    if 'exportar' in request.POST:
        detalle = 3
        proyeccion = Proyeccion(anio, tipo, departamento, puesto, nombre, detalle, plan)
        response = proyeccion.generar_excel(departamento)
        return response

    begin_time = time.time()

    proyeccion = Proyeccion(anio, tipo, departamento, puesto, nombre, detalle, plan)

    end_time = time.time()
    time_elapsed = end_time - begin_time
    print("TIEMPO PROYECCION: ", time_elapsed)

    return render(request, 'nomina/menu/proyeccion.html', { 'form': form, 'url_name': 'proyeccion', 'proyeccion': proyeccion})