from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field
from nomina.models import *
from .class_select import class_form_select_sm

class ClonarPlanForm(forms.Form):    
    descripcion = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs=class_form_select_sm), label='Nombre del nuevo plan')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('descripcion', wrapper_class="col"),
            css_class='row'),
        )
 
class PlanForm(forms.ModelForm):    
    class Meta:
        model = Plan
        fields = ['descripcion']