import time
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from nomina.models import *
from nomina.forms import *
from nomina.thread import ClonarPlanThread, EliminarPlanThread

@login_required
def planes_list(request):    
    planes = Plan.objects.all()
    context = { 'planes': planes, 'url_name': 'planes'}
    return render(request, 'nomina/planes/content.html', context)

@login_required
def plan_cargar(request, pk):
    plan = get_object_or_404(Plan, pk = pk)
    request.session['plan'] = plan.id
    request.session['plan_descripcion'] = plan.descripcion
    messages.success(request, plan.descripcion + ' cargado!')
    return redirect('planes')

@login_required
def plan_clonar(request, pk):
    plan_origen = get_object_or_404(Plan, pk = pk)
    if request.method == 'POST':
        form = ClonarPlanForm(request.POST)
        if form.is_valid():
            descripcion = form.cleaned_data['descripcion']

            plan_destino = Plan()
            plan_destino.descripcion = descripcion
            plan_destino.save()

            ClonarPlanThread(plan_origen, plan_destino).start()

            messages.success(request, 'Clonación de plan exitosa!. Dependiendo de la cantidad de información, este proceso puede tardar algunos minutos')
            return redirect('planes')
    else:
        form = ClonarPlanForm()
    return render(request, 'nomina/planes/modal-clonar.html', {'form': form, 'plan': plan_origen})

@login_required
def plan_editar(request, pk):
    plan = get_object_or_404(Plan, pk = pk)
    if request.method == 'POST':
        form = PlanForm(request.POST, instance=plan)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de plan exitosa!')
            if request.session['plan'] == plan.id:
                request.session['plan_descripcion'] = plan.descripcion
            return redirect('planes')
    else:
        form = PlanForm(instance=plan)
    return render(request, 'nomina/planes/modal-editar.html', {'form': form, 'plan': plan})

@login_required
def plan_eliminar(request, pk):
    plan = get_object_or_404(Plan, pk = pk)
    if request.is_ajax():
        context = { 'plan': plan }
        return render(request, 'nomina/planes/modal-eliminar.html', context)
    else:
        EliminarPlanThread(plan).start()
        time.sleep(2)
        messages.success(request, 'Eliminación de plan exitosa!. Dependiendo de la cantidad de información, este proceso puede tardar algunos minutos')
        return redirect('planes')
