from django import forms
from nomina.models import *
from .class_select import class_form_select_sm

class PartidaForm(forms.ModelForm):    
    class Meta:
        model = Partida
        fields = ['codigo','descripcion', 'valor', 'confianza', 'eventual', 'asimilable', 'incapacitado']
        labels = {
            'codigo': 'Código',
            'descripcion': 'Descripción',
            'valor': 'Valor',
        }

        widgets = {
            'codigo': forms.TextInput(attrs=class_form_select_sm),
            'descripcion': forms.TextInput(attrs=class_form_select_sm),
            'valor': forms.NumberInput(attrs=class_form_select_sm),
        }
