# SIGA

Sistema de Gestión y Administración

## Instalacíon de requerimentos y migrar modelos

Usar el manejador [pip](https://pip.pypa.io/en/stable/) para instalar librerias requeridas. Este comando instala la version de Django correspondiente y herramientas utilizadas en el proyecto, etc.

```bash
pip install -r requirements.txt
```

A continuación, correr las migraciones pendientes parar sincronizar con nuestra base de datos cualquier cambio en la estructura de la informaciòn del sistema

```bash
python manage.py migrate
```

## Creación de superusuario

En caso de que sea la primera vez que se implementa el sistema, es necesario correr el siguiente comando para crear el primer usuario

```bash
python manage.py createsuperuser
```

## Recolectar archivos estaticos

En caso de encontrarse en produccion, se necesita correr este comando para recolectar los archivos públicos y estáticos (JS, CSS, Imagenes, etc)

```bash
python manage.py collectstatic
```

## Ejecutar servidor

Para ejecutar el servidor es el siguiente comando

```bash
python manage.py runserver
```


Ingresa desde un navegador a [http://localhost:8000/](http://localhost:8000/) para visualizar ingresar al sistema

Para ver el panel de administración del sitio, es en el siguiente enlace [http://localhost:8000/admin](http://localhost:8000/admin)
