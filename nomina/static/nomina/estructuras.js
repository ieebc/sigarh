$(document).ready(function(){
    $('#id_tipo').on('change', function() {
        $('#loaderModal').modal('show');
        this.form.submit();
    });
    $('#id_puesto').on('change', function() {
        $('#loaderModal').modal('show');
        this.form.submit();
    });
});

$(document).on("submit", '#nuevo-form', function(e) { 
    e.preventDefault()
    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            $('#nuevoModal').modal('hide')
            $('#filtro-estructuras-form').submit();
        }
    });
});

$(document).on("submit", '#editar-form', function(e) { 
    e.preventDefault()
    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            $('#editarModal').modal('hide')
            $('#filtro-estructuras-form').submit();
        }
    });
});

$(document).on("submit", '#porcentaje-form', function(e) { 
    e.preventDefault()
    var form = $(this);
    var url = form.attr('action');

    $('#porcentajeModal').modal('hide')
    $('#loaderModal').modal('show');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {     
            $('#filtro-estructuras-form').submit();
        }
    });
});
