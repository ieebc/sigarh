from django.db import models
from .plan import Plan, DEFAULT_PLAN_ID

class ISR(models.Model):
    limite_inferior = models.FloatField(default=0)
    limite_superior = models.FloatField(default=0)
    cuota_fija = models.FloatField(default=0)
    porcentaje = models.FloatField(default=0)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return '%s - %s' % (self.limite_inferior, self.limite_superior)

    class Meta:
        ordering = ['limite_inferior']

class Subsidio(models.Model):
    limite_inferior = models.FloatField(default=0)
    limite_superior = models.FloatField(default=0)
    cuota_fija = models.FloatField(default=0)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return '%s - %s' % (self.limite_inferior, self.limite_superior)

    class Meta:
        ordering = ['limite_inferior']

class Catorcena(models.Model):
    descripcion = models.CharField(max_length=255, blank=True)
    fecha_inicio = models.DateField()
    fecha_final = models.DateField()
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return '%s - %s' % (self.fecha_inicio, self.fecha_final)

    class Meta:
        ordering = ['fecha_inicio']