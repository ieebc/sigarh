# Generated by Django 3.2.5 on 2022-01-26 21:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nomina', '0024_auto_20220125_1420'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fonacot',
            name='nss_centro_trabajo',
            field=models.CharField(max_length=11),
        ),
    ]
