$(document).ready(function(){
    $('#id_tipo_filtro').on('change', function() {
        $('#loaderModal').modal('show');
        this.form.submit();
    });
    $('#id_departamento').on('change', function() {
        $('#loaderModal').modal('show');
        this.form.submit();
    });
    $('#id_puesto_filtro').on('change', function() {
        $('#loaderModal').modal('show');
        this.form.submit();
    });
    $('#id_nombre').on('change', function() {
        $('#loaderModal').modal('show');
        this.form.submit();
    });

    $( ".empleados-eliminar" ).click(function() {
        var tableControl= document.getElementById('empleadosTable');
        var arrayOfValues = [];
        $('input:checkbox:checked', tableControl).each(function() {
            arrayOfValues.push($(this).closest('tr').attr('id'));
        }).get();

        $('#loaderModal').modal('show');  

        $.ajax({
            type: "POST",
            url: 'eliminar/',
            data: {
                empleados : JSON.stringify(arrayOfValues)
            },
            success: function(data)
            {
                $('#filtro-empleados-form').submit();
            }
        });
    });
});

$(document).on("change", '#id_tipo', function(e) { 
    actualizarSueldo(this.form)
});

$(document).on("change", '#id_puesto', function(e) { 
    actualizarSueldo(this.form)
});

function actualizarSueldo(form){
    $.ajax({
        type: "GET",
        url: 'sueldo/',
        data: {
            tipo: $("#id_tipo").val(),
            puesto: $("#id_puesto").val()
        },
        success: function(data)
        {
            $("#id_sueldo").val(data.sueldo)
        }
    });
}


$(document).on("submit", '#editar-form', function(e) { 
    e.preventDefault()
    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            $('#editarModal').modal('hide')
            $('#loaderModal').modal('show');
            $('#filtro-empleados-form').submit();
        }
    });
});

$(document).on("submit", '#nuevo-form', function(e) { 
    e.preventDefault()
    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            $('#nuevoModal').modal('hide')
            $('#filtro-empleados-form').submit();
        }
    });
});
