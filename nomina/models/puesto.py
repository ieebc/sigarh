from django.db import models
from .plan import Plan, DEFAULT_PLAN_ID

class Puesto(models.Model):
    descripcion = models.CharField(max_length=255)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return self.descripcion

    class Meta:
        ordering = ['descripcion']