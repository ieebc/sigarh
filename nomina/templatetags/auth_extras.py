from django import template
from django.db.models import Q
from django.contrib.humanize.templatetags.humanize import intcomma
import decimal

register = template.Library()

@register.filter(name='has_group') 
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists() 

@register.filter(name='es_rh') 
def es_rh(user):
    return user.groups.filter(Q(name='RH') | Q(name='Sistemas')).exists() 

@register.filter(name='es_administracion') 
def es_administracion(user):
    return user.groups.filter(Q(name='Administracion') | Q(name='Sistemas')).exists()

@register.filter(name='es_sistemas') 
def es_sistemas(user):
    return user.groups.filter(Q(name='Sistemas')).exists() 

@register.filter(name='es_moneda') 
def es_moneda(value):
    return (isinstance(value, decimal.Decimal))
    
@register.filter(name='moneda') 
def moneda(pesos):
    pesos = round(float(pesos), 2)
    return "$%s%s" % (intcomma(int(pesos)), ("%0.2f" % pesos)[-3:])
