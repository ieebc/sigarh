from django.db import models
from .plan import Plan, DEFAULT_PLAN_ID

class Departamento(models.Model):
    descripcion = models.CharField(max_length=255)
    orden = models.IntegerField(default=0)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return self.descripcion

    class Meta:
        ordering = ['orden']