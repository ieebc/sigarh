from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field
from crispy_forms.bootstrap import AppendedText
from nomina.models import *
from .class_select import class_form_select_sm

class EstructuraForm(forms.ModelForm):    
    class Meta:
        model = Estructura
        fields = ['tipo','rama', 'nivel', 'puesto', 'clave', 'nivel_consecutivo', 'nivel_tabular', 'sueldo']
        labels = {
            'tipo': 'Tipo',
            'rama': 'Rama',
            'nivel': 'Nivel Organizacional',
            'puesto': 'Puesto',
            'clave': 'Clave',
            'nivel_consecutivo': 'Nivel Consecutivo',
            'nivel_tabular': 'Nivel Tabular',
            'sueldo': 'Sueldo',
        }

        widgets = {
            'tipo': forms.Select(attrs=class_form_select_sm),
            'rama': forms.Select(attrs=class_form_select_sm),
            'nivel': forms.Select(attrs=class_form_select_sm),
            'puesto': forms.Select(attrs=class_form_select_sm),
            'clave': forms.TextInput(attrs=class_form_select_sm),
            'sueldo': forms.NumberInput(attrs=class_form_select_sm),
            'nivel_tabular': forms.NumberInput(attrs=class_form_select_sm),
            'nivel_consecutivo': forms.NumberInput(attrs=class_form_select_sm),
        }

    def __init__(self, plan, *args, **kwargs):
        super(EstructuraForm, self).__init__(*args, **kwargs)
        self.fields['puesto'].queryset = Puesto.objects.filter(plan = plan)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('tipo', wrapper_class="col"),
                Field('rama', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('nivel', wrapper_class="col"),
                Field('puesto', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('clave', wrapper_class="col"),
                Field('nivel_consecutivo', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('nivel_tabular', wrapper_class="col"),
                Field('sueldo', wrapper_class="col"),
                css_class='row'),
        )

class EstructuraFiltro(forms.Form):
    tipo = forms.ModelChoiceField(label='Tipo', queryset=TipoEmpleado.objects.all(),widget=forms.Select(attrs=class_form_select_sm), empty_label="<TODOS>", required=False)
    puesto = forms.ModelChoiceField(label='Puesto', queryset=None,widget=forms.Select(attrs=class_form_select_sm), empty_label="<TODOS>", required=False)

    def __init__(self, plan, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['puesto'].queryset = Puesto.objects.filter(plan = plan)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('tipo', wrapper_class="col"),
                Field('puesto', wrapper_class="col"),
            css_class='row'),

        )

class EstructuraSueldosForm(forms.Form):
    porcentaje = forms.DecimalField(label='Porcentaje',widget=forms.NumberInput(attrs=class_form_select_sm), required=True, initial=0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                AppendedText('porcentaje', '%'),
            css_class='row'),
        )
