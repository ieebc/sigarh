from django import forms
from nomina.models import *
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Row, Field, Fieldset, LayoutObject, TEMPLATE_PACK 
from crispy_forms.bootstrap import PrependedAppendedText
from .class_select import class_form_select_sm
from .inputs import DateInput


widget_text = forms.TextInput(attrs=class_form_select_sm)

class FonacotForm(forms.ModelForm):    
    class Meta:
        model = Fonacot
        exclude = []
        widgets = {
            'clave_movimiento': forms.Select(attrs=class_form_select_sm),
            'rfc': widget_text,
            'indicador_seguro': forms.Select(attrs=class_form_select_sm),
            'nss': widget_text,
            'curp': widget_text,
            'sexo': forms.Select(attrs=class_form_select_sm),
            'nombre': widget_text,
            'apellido_paterno': widget_text,
            'apellido_materno': widget_text,
            'nombre_trabajador': widget_text,
            'fecha_ingreso': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'salario_mensual_base': forms.NumberInput(attrs=class_form_select_sm),
            'salario_mensual_bruto': forms.NumberInput(attrs=class_form_select_sm),
            'nss_centro_trabajo': widget_text,
            'no_fonacot': widget_text,
            'no_trabajador': widget_text,
            'salario_mensual_neto': forms.NumberInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
        super(FonacotForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('clave_movimiento', wrapper_class="col"),
                Field('rfc', wrapper_class="col"),
                Field('indicador_seguro', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('nss', wrapper_class="col"),
                Field('curp', wrapper_class="col"),
                Field('sexo', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('nombre', wrapper_class="col"),
                Field('apellido_paterno', wrapper_class="col"),
                Field('apellido_materno', wrapper_class="col"),
                css_class='row'),
            Div(
                
                Field('nombre_trabajador', wrapper_class="col"),
                Field('fecha_ingreso', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('salario_mensual_base', wrapper_class="col"),
                Field('salario_mensual_bruto', wrapper_class="col"),
                Field('salario_mensual_neto', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('nss_centro_trabajo', wrapper_class="col"),
                Field('no_fonacot', wrapper_class="col"),
                Field('no_trabajador', wrapper_class="col"),
                css_class='row'),
        )