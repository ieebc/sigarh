from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field
from nomina.models import *
from .class_select import class_form_select_sm

ANIOS =(
    ("2020", "2020"),
    ("2021", "2021"),
    ("2022", "2022"),
)

DETALLE =(
    (1, "Por Total"),
    (2, "Por Departamentos"),
    (3, "Por Empleados"),
)

class ProyeccionForm(forms.Form):
    anio = forms.ChoiceField(label='Año',choices=ANIOS,widget=forms.Select(attrs=class_form_select_sm), initial='2022')
    tipo = forms.ModelChoiceField(label='Tipo', queryset=TipoEmpleado.objects.all(),widget=forms.Select(attrs=class_form_select_sm), initial=0)
    departamento = forms.ModelChoiceField(label='Departamento', queryset=None, widget=forms.Select(attrs=class_form_select_sm), empty_label="<TODOS>", required=False)
    puesto = forms.ModelChoiceField(label='Puesto', queryset=None, widget=forms.Select(attrs=class_form_select_sm), empty_label="<TODOS>", required=False)
    nombre = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))
    detalle = forms.ChoiceField(label='Detalle',choices=DETALLE,widget=forms.Select(attrs=class_form_select_sm), initial=1)

    def __init__(self, plan, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['departamento'].queryset = Departamento.objects.filter(plan = plan)
        self.fields['puesto'].queryset = Puesto.objects.filter(plan = plan)

        self.helper = FormHelper(self)
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('anio', wrapper_class="col"),
                Field('tipo', wrapper_class="col"),
            css_class='row'),

            Div(
                Field('departamento', wrapper_class="col"),
                Field('puesto', wrapper_class="col"),
            css_class='row'),

            Div(
                Field('nombre', wrapper_class="col"),
                Field('detalle', wrapper_class="col"),
            css_class='row'),
        )
