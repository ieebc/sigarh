from django.db import models


class Fonacot(models.Model):

    MOVIMIENTOS = [
        (1, 'ALTA O REINGRESO'),
        (2, 'BAJA'),
        (3, 'CAMBIO DE SALARIO'),
    ]

    INDICADORES_SEGURO = [
            (1, 'IMSS'),
            (2, 'ISSTE'),
            (3, 'ISSEMYM'),
            (4, 'OTRO'),
        ]

    SEXOS = [
            (1, 'MASCULINO'),
            (2, 'FEMENINO'),
        ]

    clave_movimiento = models.IntegerField(choices=MOVIMIENTOS, default=1)
    rfc = models.CharField(max_length=13)
    indicador_seguro = models.IntegerField(choices=INDICADORES_SEGURO, default=1)
    nss = models.CharField(max_length=11)
    curp = models.CharField(max_length=18)
    sexo = models.IntegerField(choices=SEXOS, default=1)
    nombre = models.CharField(max_length=40)
    apellido_paterno = models.CharField(max_length=20, blank=True)
    apellido_materno = models.CharField(max_length=20, blank=True)
    nombre_trabajador = models.CharField(max_length=20, blank=True)
    fecha_ingreso = models.DateField(null=True, blank=True)
    salario_mensual_base = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    salario_mensual_bruto = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    nss_centro_trabajo = models.CharField(max_length=11)
    no_fonacot = models.CharField(max_length=10, blank=True)
    no_trabajador = models.CharField(max_length=11)
    salario_mensual_neto = models.DecimalField(max_digits=9, decimal_places=2, default=0)

    def get_cadena(self):
        cadena = self.get_clave_movimiento()
        cadena += self.get_rfc()
        cadena += self.get_indicador_seguro()
        cadena += self.get_nss()
        cadena += self.get_curp()
        cadena += self.get_sexo()
        cadena += self.get_nombre()
        cadena += self.get_apellido_paterno()
        cadena += self.get_apellido_materno()
        cadena += self.get_nombre_trabajador()
        cadena += self.get_fecha_ingreso()
        cadena += self.get_salario_mensual_base()
        cadena += self.get_salario_mensual_bruto()
        cadena += self.get_nss_centro_trabajo()
        cadena += self.get_no_fonacot()
        cadena += self.get_no_trabajador()
        cadena += self.get_salario_mensual_neto()
        return cadena

    def get_clave_movimiento(self):
        return normalizar(str(self.clave_movimiento), 1)
    
    def get_rfc(self):
        return normalizar(str(self.rfc), 13)

    def get_indicador_seguro(self):
        return normalizar(str(self.indicador_seguro), 1)

    def get_nss(self):
        return normalizar(str(self.nss), 11)

    def get_curp(self):
        return normalizar(str(self.curp), 18)

    def get_sexo(self):
        return normalizar(str(self.sexo), 1)

    def get_nombre(self):
        return normalizar(str(self.nombre), 40)

    def get_apellido_paterno(self):
        return normalizar(str(self.apellido_paterno), 20)

    def get_apellido_materno(self):
        return normalizar(str(self.apellido_materno), 20)

    def get_nombre_trabajador(self):
        return normalizar(str(self.nombre_trabajador), 20)

    def get_fecha_ingreso(self):
        return normalizar(self.fecha_ingreso.strftime("%m/%d/%Y"), 10)

    def get_salario_mensual_base(self):
        return normalizar(str(self.salario_mensual_base).zfill(9), 9)

    def get_salario_mensual_bruto(self):
        return normalizar(str(self.salario_mensual_bruto).zfill(9), 9)

    def get_nss_centro_trabajo(self):
        return normalizar(str(self.nss_centro_trabajo), 11)
    
    def get_no_fonacot(self):
        return normalizar(str(self.no_fonacot), 10)

    def get_no_trabajador(self):
        if not str(self.no_trabajador).strip():
            return normalizar(str(self.rfc)[0:11], 11)
        return normalizar(str(self.no_trabajador), 11)

    def get_salario_mensual_neto(self):
        return normalizar(str(self.salario_mensual_neto).zfill(9), 9)

    def falta_informacion(self):
        if not str(self.rfc).strip():
            return 1
        if not str(self.nss).strip():
            return 2
        if self.salario_mensual_base == 0:
            return 3
        if self.salario_mensual_bruto == 0:
            return 4
        if self.salario_mensual_neto == 0:
            return 5
        return 0

def quitar_acentos(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        s = s.replace(a, b).replace(a.upper(), b.upper())
    return s

def normalizar(valor, longitud):
    if len(valor) == longitud:
        return quitar_acentos(valor)
    if len(valor) < longitud:
        return quitar_acentos(valor.ljust(longitud))
    if len(valor) > longitud:
        return quitar_acentos(valor[0:longitud])