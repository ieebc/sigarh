from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def isrs_list(request):    
    plan = request.session['plan']
    isrs = ISR.objects.filter(plan = plan)
    context = { 'isrs': isrs, 'url_name': 'isrs'}
    return render(request, 'nomina/isrs/content.html', context)

@login_required
def isr_nuevo(request):
    plan = request.session['plan']
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = ISRForm(request.POST)
        if form.is_valid():
            isr = form.save(commit=False)
            isr.user = current_user
            isr.plan = Plan.objects.filter(id = plan).first()
            isr.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('isrs')
    else:
        form = ISRForm()
    return render(request, 'nomina/isrs/modal-nuevo.html', {'form': form})

@login_required
def isr_editar(request, pk):
    isr = get_object_or_404(ISR, pk = pk)
    if request.method == 'POST':
        form = ISRForm(request.POST, instance=isr)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de registro ISR exitosa!')
            return redirect('isrs')
    else:
        form = ISRForm(instance=isr)
    return render(request, 'nomina/isrs/modal-editar.html', {'form': form, 'isr': isr})

@login_required
def isr_eliminar(request, pk):
    isr = get_object_or_404(ISR, pk = pk)
    if request.is_ajax():
        context = { 'isr': isr }
        return render(request, 'nomina/isrs/modal-eliminar.html', context)
    else:
        isr.delete()
        messages.success(request, 'Eliminación de registro ISR exitosa!')
        return redirect('isrs')