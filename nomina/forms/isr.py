from django import forms
from nomina.models import *
from .class_select import class_form_select_sm

class ISRForm(forms.ModelForm):    
    class Meta:
        model = ISR
        fields = ['limite_inferior','limite_superior', 'cuota_fija', 'porcentaje']
        labels = {
            'limite_inferior': 'Límite Inferior',
            'limite_superior': 'Límite Superior',
            'cuota_fija': 'Cuota Fija',
            'porcentaje': 'Porcentaje',
        }

        widgets = {
            'limite_inferior': forms.NumberInput(attrs=class_form_select_sm),
            'limite_superior': forms.NumberInput(attrs=class_form_select_sm),
            'cuota_fija': forms.NumberInput(attrs=class_form_select_sm),
            'porcentaje': forms.NumberInput(attrs=class_form_select_sm),
        }
