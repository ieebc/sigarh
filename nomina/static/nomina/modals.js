$(document).ready(function(){
    feather.replace()

    //MODALS
    $( ".nuevo-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#nuevoModal').modal('show')
            },
        });
    });

    $( ".editar-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#editarModal').modal('show')
            },
        });   
    });

    $( ".eliminar-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#eliminarModal').modal('show')
            },
        });   
    });

    $( ".clonar-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#clonarModal').modal('show')
            },
        });   
    });

    $( ".porcentaje-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#porcentajeModal').modal('show')
            },
        });   
    });

    $(".sideopt" ).click(function() {
        $('#loaderModal').modal('show');
    });
});