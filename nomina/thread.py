import threading
from nomina.models import *

class ClonarPlanThread(threading.Thread):
    def __init__(self, plan_origen, plan_destino):
        self.plan_origen = plan_origen
        self.plan_destino = plan_destino
        threading.Thread.__init__(self)

    def run(self):
        try:
            departamentos = Departamento.objects.filter(plan = self.plan_origen)
            puestos = Puesto.objects.filter(plan = self.plan_origen)
            estructuras = Estructura.objects.filter(plan = self.plan_origen)
            empleados = Empleado.objects.filter(plan = self.plan_origen)            
            partidas = Partida.objects.filter(plan = self.plan_origen)
            isrs = ISR.objects.filter(plan = self.plan_origen)
            subsidios = Subsidio.objects.filter(plan = self.plan_origen)
            catorcenas = Catorcena.objects.filter(plan = self.plan_origen)

            departamentos_dict = {}
            puestos_dict = {}

            # Importar Partidas
            for partida in partidas:
                partida.pk = None
                partida.plan = self.plan_destino
                partida.save()

            #Catorcenas
            for catorcena in catorcenas:
                catorcena.pk = None
                catorcena.plan = self.plan_destino
                catorcena.save()

            #Tabla ISR
            for isr in isrs:
                isr.pk = None
                isr.plan = self.plan_destino
                isr.save()
            
            #Tabla Subsidio
            for subsidio in subsidios:
                subsidio.pk = None
                subsidio.plan = self.plan_destino
                subsidio.save()

            # Importar Departamentos
            for departamento in departamentos:
                departamento_origen = departamento.id
                departamento.pk = None
                departamento.plan = self.plan_destino
                departamento.save()
                departamentos_dict[departamento_origen] = departamento

            # Importar Puesto
            for puesto in puestos:
                puesto_origen = puesto.id
                puesto.pk = None
                puesto.plan = self.plan_destino
                puesto.save()     
                puestos_dict[puesto_origen] = puesto     

            # Importar Estructuras
            for estructura in estructuras:
                estructura.pk = None
                estructura.plan = self.plan_destino
                estructura.puesto = puestos_dict[estructura.puesto.id]
                estructura.save()

            # Importar Empleados y Periodos
            for empleado in empleados:
                periodos = Periodo.objects.filter(empleado = empleado)
                empleado.pk = None
                empleado.plan = self.plan_destino
                empleado.puesto = puestos_dict[empleado.puesto.id]
                empleado.departamento = departamentos_dict[empleado.departamento.id]
                empleado.save()

                for periodo in periodos:
                    periodo.pk = None
                    periodo.plan = self.plan_destino
                    periodo.empleado = empleado
                    periodo.save()
            
            self.plan_destino.clonado = True
            self.plan_destino.save()

        except Exception as e:
            print(e)

class EliminarPlanThread(threading.Thread):
    def __init__(self, plan):
        self.plan = plan
        threading.Thread.__init__(self)

    def run(self):
        try:
            self.plan.eliminando = True
            self.plan.save()
            
            Empleado.objects.filter(plan = self.plan).delete()
            Partida.objects.filter(plan = self.plan).delete()
            Estructura.objects.filter(plan = self.plan).delete()
            Departamento.objects.filter(plan = self.plan).delete()
            Puesto.objects.filter(plan = self.plan).delete()
            ISR.objects.filter(plan = self.plan).delete()
            Subsidio.objects.filter(plan = self.plan).delete()
            Catorcena.objects.filter(plan = self.plan).delete()

            self.plan.delete()

        except Exception as e:
            print(e)
