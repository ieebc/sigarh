import os
import sys
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from nomina.models import *
from nomina.forms import *

import csv

#import pyodbc

# Create your views here.
@login_required
def scripts(request):
    if request.user.is_authenticated:
        context = { 'url_name': 'scripts'}
        return render(request, 'nomina/menu/scripts.html', context)
    else:
        return redirect('login')

def scripts_fonacot_csv(request):
    if request.user.is_authenticated:
        with open(os.path.join(sys.path[0], "fonacot.csv"), encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    print(row)
                    no_empleado = row[0]
                    nombre_empleado = row[1]
                    fecha_ingreso = row[2]
                    rfc = row[3]
                    curp = row[4]
                    nss = row[6]
                    salario = row[7]
                    sexo = row[8]

                    f = Fonacot()
                    f.no_trabajador = no_empleado
                    f.nombre = nombre_empleado
                    f.fecha_ingreso = fecha_ingreso
                    f.rfc = rfc
                    f.curp = curp
                    f.nss = nss
                    if salario.replace(',','.').strip():
                        f.salario_mensual_base = salario.replace(',','.')
                        f.salario_mensual_bruto = salario.replace(',','.')

                    if sexo == 'H':
                        f.sexo = 1
                    else:
                        f.sexo = 2

                    #Defaults
                    f.clave_movimiento = 1
                    f.indicador_seguro = 4
                    f.nss_centro_trabajo = 'convenioiss'

                    print(f)
                    f.save()


                    print(no_empleado, nombre_empleado, fecha_ingreso, rfc, curp, nss, salario)
                    line_count += 1
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_fonacot(request):
    if request.user.is_authenticated:
        conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.131;PORT=1433;DATABASE=PBR_2020;UID=acceso;PWD=dell2016;')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM partidas_fonacot ORDER by Rfc_y_Homoclave')

        for i in cursor:
            fonacots = Fonacot.objects.filter(rfc = i[1]).count()
            if fonacots == 0:
                fonacot = Fonacot()
                fonacot.clave_movimiento = i[0]
                fonacot.rfc = i[1]
                fonacot.indicador_seguro = i[2]
                fonacot.nss = i[3]
                fonacot.curp = i[4]
                fonacot.sexo = i[5]
                fonacot.nombre = i[6]
                fonacot.apellido_paterno = i[7]
                fonacot.apellido_materno = i[8]
                fonacot.nombre_trabajador = i[9]
                fonacot.fecha_ingreso = i[10]
                fonacot.salario_mensual_base = i[11]
                fonacot.salario_mensual_bruto = i[12]
                fonacot.nss_centro_trabajo = i[13]
                fonacot.no_fonacot = i[14]
                fonacot.no_trabajador = i[15]
                fonacot.salario_mensual_neto = i[16]
                #print(fonacot.fecha_ingreso)
                fonacot.save()

        messages.success(request, 'Registro fonacot importados!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_departamentos(request):
    if request.user.is_authenticated:
        conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.131;PORT=1433;DATABASE=PBR_2020;UID=acceso;PWD=dell2016;')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM partidas_departamentos ORDER by nivel')

        for i in cursor:
            departamentos = Departamento.objects.filter(descripcion = i[1]).count()
            if departamentos == 0:
                departamento = Departamento()
                departamento.descripcion = i[1]
                departamento.orden = i[4]
                departamento.save()

        messages.success(request, 'Departamentos importados!')
        return redirect('scripts')
    else:
        return redirect('login')
        
def scripts_puestos(request):
    if request.user.is_authenticated:
        conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.131;PORT=1433;DATABASE=PBR_2020_PLAN_B;UID=acceso;PWD=dell2016;')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM partidas_puestos')

        for i in cursor:
            puestos = Puesto.objects.filter(descripcion = i[1]).count()
            if puestos == 0:
                puesto = Puesto()
                puesto.descripcion = i[1]
                puesto.save()
                print("Crea", puesto)

        messages.success(request, 'Puestos importados!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_estructuras(request):
    plan = request.session['plan']
    if request.user.is_authenticated:
        conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.131;PORT=1433;DATABASE=PBR_2020_PLAN_B;UID=acceso;PWD=dell2016;')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM Partidas_Extructura')

        for i in cursor:
            tipo_desc = i[1]
            clave = i[3]
            rama = i[4]
            nivel = i[5]
            consecutivo = i[6]
            tabular = i[7]
            sueldo_2020 = i[8]
            sueldo = i[9]
            puesto_desc = i[10]

            if rama == '':
                rama = 'NA'

            if puesto_desc == 'SUPERVISORES ELECTORALES':
                puesto_desc = 'SUPERVISOR ELECTORAL'

            if tipo_desc == 'CF':
                tipo_desc = "Confianza"
            if tipo_desc == 'EV':
                tipo_desc = "Eventual"
            if tipo_desc == 'AS':
                tipo_desc = "Asimilable"
            if tipo_desc == 'IN':
                tipo_desc = "Incapacitado"

            estructuras = Estructura.objects.filter(Q(tipo__descripcion = tipo_desc), Q(puesto__descripcion = puesto_desc), plan = plan).count()
            if estructuras == 0:
                tipo = TipoEmpleado.objects.filter(descripcion = tipo_desc).first()
                rama = Rama.objects.filter(descripcion = rama).first()
                nivel = Nivel.objects.filter(descripcion = nivel).first()
                puesto = Puesto.objects.filter(descripcion = puesto_desc).first()

                if tipo and rama and nivel and puesto:
                    estructura = Estructura()
                    estructura.tipo = tipo
                    estructura.rama = rama
                    estructura.nivel = nivel
                    estructura.puesto = puesto
                    estructura.clave = clave
                    estructura.nivel_consecutivo = consecutivo
                    estructura.nivel_tabular = tabular
                    estructura.sueldo = sueldo_2020
                    estructura.plan = Plan.objects.filter(id = plan).first()
                    estructura.save()

        messages.success(request, 'Estructuras importadas!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_empleados(request):
    plan = request.session['plan']
    if request.user.is_authenticated:
        conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.131;PORT=1433;DATABASE=PBR_2020_PLAN_B;UID=acceso;PWD=dell2016;')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM partidas_empleados, partidas_departamentos, partidas_puestos WHERE (EstadoEmpleado = 'A' or EstadoEmpleado = 'R') AND FechaAlta IS NOT NULL AND partidas_empleados.IDDepartamento = partidas_departamentos.IDDepartamento AND partidas_empleados.IDPuesto = partidas_puestos.IDPuesto")

        empleados = cursor.fetchall()
        for row_emp in empleados:
            numero = row_emp[4]
            nombre = row_emp[5]
            paterno = row_emp[6]
            materno = row_emp[7]
            estado = row_emp[12]
            fecha_alta = row_emp[16]
            tipo_desc = row_emp[17]
            vacaciones = row_emp[22]
            excedente = row_emp[23]
            fecha_reingreso = row_emp[25]
            spen = row_emp[29]
            depa_desc = row_emp[31]
            puesto_desc = row_emp[37]

            if tipo_desc == 'C':
                tipo_desc = "Confianza"
            if tipo_desc == 'E':
                tipo_desc = "Eventual"
            if tipo_desc == 'A':
                tipo_desc = "Asimilable"
            if tipo_desc == 'I':
                tipo_desc = "Incapacitado"

            if not spen:
                spen = False

            #Si no existe el empleado con ese codigo / numero, se agrega
            empleado = Empleado.objects.filter(Q(numero = numero), plan = plan).first()
            if not empleado:
                tipo = TipoEmpleado.objects.filter(descripcion = tipo_desc).first()
                departamento = Departamento.objects.filter(descripcion = depa_desc).first()
                puesto = Puesto.objects.filter(descripcion = puesto_desc).first()

                if tipo and departamento and puesto:
                    empleado = Empleado()
                    empleado.tipo = tipo
                    empleado.departamento = departamento
                    empleado.puesto = puesto
                    empleado.numero = numero
                    empleado.nombre = nombre
                    empleado.apellido_paterno = paterno
                    empleado.apellido_materno = materno
                    empleado.vacaciones = vacaciones
                    empleado.excedente = excedente
                    empleado.spen = spen
                    empleado.plan = Plan.objects.filter(id = plan).first()
                    empleado.save()

                    #print(empleado)

                    #Periodos del empleado
                    if empleado.tipo.es_confianza():
                        #Periodos de confianza
                        fecha_ingreso = None
                        if estado == 'A':
                            if fecha_alta:
                                fecha_ingreso = fecha_alta.date()

                        else:
                            if fecha_reingreso:
                                fecha_ingreso = fecha_reingreso.date()
                            
                        if not Periodo.objects.filter(empleado = empleado, fecha_ingreso = fecha_ingreso).first():
                            periodo = Periodo()
                            periodo.empleado = empleado
                            periodo.fecha_ingreso = fecha_ingreso
                            periodo.save()

                    else:
                        #Periodos de eventuales / asimilables
                        cursor.execute("SELECT * FROM partidas_periodos WHERE CodigoEmpleado = '%s' ORDER BY Numero ASC" % empleado.numero)
                        periodos = cursor.fetchall()
                        for row_per in periodos:
                            if row_per[1] and row_per[2]:
                                if not Periodo.objects.filter(empleado = empleado, fecha_ingreso = row_per[1].date(), fecha_baja = row_per[2].date()).first():
                                    periodo = Periodo()
                                    periodo.empleado = empleado
                                    periodo.fecha_ingreso = row_per[1].date()
                                    periodo.fecha_baja = row_per[2].date()
                                    periodo.save()
            else:
                print("Empleado no encontrado")


        messages.success(request, 'Empleados importados!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_partidas(request):
    plan = request.session['plan']
    partidas = Partida.objects.all()
    plan_obj = Plan.objects.filter(id = plan).first()

    for partida in partidas:
        part_plan = Partida()
        part_plan.codigo = partida.codigo
        part_plan.descripcion = partida.descripcion
        part_plan.valor = partida.valor
        part_plan.confianza = partida.confianza
        part_plan.eventual = partida.eventual
        part_plan.asimilable = partida.asimilable
        part_plan.incapacitado = partida.incapacitado
        part_plan.orden = partida.orden
        part_plan.plan = plan_obj
        part_plan.save()
    
    messages.success(request, 'Partidas importadas!')
    return redirect('scripts')

def scripts_departamentos_plan(request):
    if request.user.is_authenticated:
        plan = request.session['plan']        
        plan = Plan.objects.filter(id = plan).first()

        plan_a = Plan.objects.filter(id = 1).first()

        departamentos_a = Departamento.objects.filter(plan = plan_a)

        for departamento in departamentos_a:
            departamento.pk = None
            departamento.plan = plan
            departamento.save()

        messages.success(request, 'Departamentos importados!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_puestos_plan(request):
    if request.user.is_authenticated:
        plan = request.session['plan']        
        plan = Plan.objects.filter(id = plan).first()

        plan_a = Plan.objects.filter(id = 1).first()

        puestos_a = Puesto.objects.filter(plan = plan_a)

        for puesto in puestos_a:
            puesto.pk = None
            puesto.plan = plan
            puesto.save()
            
        messages.success(request, 'Puestos importados!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_plan_sincronizar(request):
    if request.user.is_authenticated:
        plan = request.session['plan']        
        plan = Plan.objects.filter(id = plan).first()

        estructuras = Estructura.objects.filter(plan = plan)
        empleados = Empleado.objects.filter(plan = plan)

        for estructura in estructuras:
            puesto = Puesto.objects.filter(plan = plan, descripcion = estructura.puesto.descripcion).first()
            estructura.puesto = puesto
            estructura.save()

        for empleado in empleados:
            puesto = Puesto.objects.filter(plan = plan, descripcion = empleado.puesto.descripcion).first()
            departamento = Departamento.objects.filter(plan = plan, descripcion = empleado.departamento.descripcion).first()
            empleado.puesto = puesto
            empleado.departamento = departamento
            empleado.save()
            
        messages.success(request, 'Plan Sincronizado!')
        return redirect('scripts')
    else:
        return redirect('login')

def scripts_plan_sueldos(request):
    if request.user.is_authenticated:
        plan = request.session['plan']        
        plan = Plan.objects.filter(id = plan).first()

        estructuras_a = Estructura.objects.filter(plan = 1)

        for estructura_a in estructuras_a:
            puesto_b = Puesto.objects.filter(plan = 13, descripcion = estructura_a.puesto.descripcion).first()
            estructura_b = Estructura.objects.filter(plan = 13, tipo = estructura_a.tipo, puesto = puesto_b).first()
            if estructura_b:
                estructura_a.sueldo = estructura_b.sueldo
                estructura_a.save()
            
        messages.success(request, 'Plan Sincronizado!')
        return redirect('scripts')
    else:
        return redirect('login')

@login_required
def scripts_plan_isr(request):
    plan = request.session['plan']        
    plan = Plan.objects.filter(id = plan).first()

    isrs_a = ISR.objects.filter(plan = 1)
    subsidios_a = Subsidio.objects.filter(plan = 1)

    for isr in isrs_a:
        isr.pk = None
        isr.plan = plan
        isr.save()
    
    for sub in subsidios_a:
        sub.pk = None
        sub.plan = plan
        sub.save()
            
    messages.success(request, 'Plan Sincronizado!')
    return redirect('scripts')

def scripts_asimilables(request):
    plan = request.session['plan']        
    plan = Plan.objects.filter(id = plan).first()

    empleados = Empleado.objects.filter(plan = plan, tipo__descripcion__contains = 'Asimilable')


    for empleado in empleados:
        if empleado.fecha_ingreso.year >= 2022:
            print(empleado)
        else:
            empleado.delete()
            
    messages.success(request, 'Plan Sincronizado!')
    return redirect('scripts')

def scripts_consejeros(request):
    plan = request.session['plan']        
    plan = Plan.objects.filter(id = plan).first()

    empleados = Empleado.objects.filter(plan = plan, tipo__descripcion__contains = 'Confianza').exclude(puesto__descripcion__contains = 'CONSEJERO')


    for empleado in empleados:
        empleado.delete()

            
    messages.success(request, 'Plan Sincronizado!')
    return redirect('scripts')