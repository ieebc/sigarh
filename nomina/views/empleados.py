import json
import xlwt
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView
from django.db import transaction
from django.urls import reverse_lazy
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from nomina.models import *
from nomina.forms import EmpleadoFiltro, EmpleadoForm, PeriodoFormset


@login_required
def empleados_list(request):        
    plan = request.session['plan']
    tipo = TipoEmpleado.objects.filter(descripcion = "Confianza").first()
    departamento = None
    puesto = None
    nombre = ""

    if request.method == 'POST':
        form = EmpleadoFiltro(plan, request.POST)
        if form.is_valid():
            tipo = form.cleaned_data['tipo']
            departamento = form.cleaned_data['departamento']
            puesto = form.cleaned_data['puesto']
            nombre = form.cleaned_data['nombre']
    else:
        form = EmpleadoFiltro(plan)
            
    empleados = filtro_empleados(tipo, departamento, puesto, nombre, plan)
    context = { 'empleados': empleados, 'url_name': 'empleados', 'form': form}
    return render(request, 'nomina/empleados/content.html', context)

@login_required
def sueldo(request):
    sueldo = 0
    tipo = request.GET["tipo"]
    puesto = request.GET["puesto"]
    estructura = Estructura.objects.filter(tipo = tipo, puesto = puesto).first()
    if estructura:
        sueldo = estructura.sueldo
    return JsonResponse({"sueldo": sueldo}, status=200)

class EmpleadoCreateView(CreateView):
    model = Empleado
    form_class = EmpleadoForm
    template_name = 'nomina/empleados/modal-nuevo.html'
    success_url = None

    def get_form(self, *args, **kwargs):
        plan = self.request.session['plan']
        form = super().get_form(*args, **kwargs)
        form.fields['departamento'].queryset = Departamento.objects.filter(plan = plan)
        form.fields['puesto'].queryset = Puesto.objects.filter(plan = plan)
        return form

    def get_context_data(self, **kwargs):
        data = super(EmpleadoCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['periodos'] = PeriodoFormset(self.request.POST)
        else:
            data['periodos'] = PeriodoFormset()
        return data

    def form_valid(self, form):
        plan = self.request.session['plan']
        context = self.get_context_data()
        periodos = context['periodos']

        with transaction.atomic():
            self.object = form.save()
            self.object.plan = Plan.objects.filter(id = plan).first()
            if periodos.is_valid():
                periodos.instance = self.object
                periodos.save()

        return super(EmpleadoCreateView, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, 'Creación de empleado exitosa!')
        return reverse_lazy('empleados')

class EmpleadoUpdateView(UpdateView):
    model = Empleado
    form_class = EmpleadoForm
    template_name = 'nomina/empleados/modal-editar.html'
    success_url = None

    def get_form(self, *args, **kwargs):
        plan = self.request.session['plan']
        form = super().get_form(*args, **kwargs)
        form.fields['departamento'].queryset = Departamento.objects.filter(plan = plan)
        form.fields['puesto'].queryset = Puesto.objects.filter(plan = plan)
        tipo = self.object.tipo
        puesto = self.object.puesto
        estructura = Estructura.objects.filter(plan = plan, tipo = tipo, puesto = puesto).first()
        if estructura:
            form.fields['sueldo'].initial = estructura.sueldo
        return form

    def get_context_data(self, **kwargs):
        data = super(EmpleadoUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['periodos'] = PeriodoFormset(self.request.POST, instance = self.object)
        else:
            data['periodos'] = PeriodoFormset(instance = self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        periodos = context['periodos']
        
        with transaction.atomic():
            self.object = form.save()
            if periodos.is_valid():
                periodos.instance = self.object
                periodos.save()

        return super(EmpleadoUpdateView, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, 'Edición de empleado exitosa!')
        return reverse_lazy('empleados')


@login_required
def empleado_eliminar(request, pk):
    empleado = get_object_or_404(Empleado, pk = pk)
    if request.is_ajax():
        context = { 'empleado': empleado }
        return render(request, 'nomina/empleados/modal-eliminar.html', context)
    else:
        empleado.delete()
        messages.success(request, 'Eliminación de empleado exitosa!')
        return redirect('empleados')

@login_required
@csrf_exempt
def empleados_eliminar(request):
    empleados = json.loads(request.POST['empleados'])
    Empleado.objects.filter(pk__in=empleados).delete()
        
    messages.success(request, 'Eliminación de empleados exitosa!')
    return redirect('empleados')

@login_required
def empleados_reporte(request):
    plan = request.session['plan']
    empleados = Empleado.objects.filter(plan = plan)

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="EMPLEADOS_DIAS_VACACIONES.xls"'
    libro = xlwt.Workbook(encoding='utf-8', style_compression=2)
    hoja = libro.add_sheet('RPT') 

    
    catorcenas = Catorcena.objects.filter(plan = plan, fecha_final__year = 2022)

    hoja.write(0, 0, "Tipo")
    hoja.write(0, 1, "Número")
    hoja.write(0, 2, "Nombre")
    hoja.write(0, 3, "Departamento")
    hoja.write(0, 4, "Puesto")
    hoja.write(0, 5, "Días Vacaciones")

    row_num = 1
    for empleado in empleados:
        empleado.set_data_emp(2022, catorcenas)
        hoja.write(row_num, 0, str(empleado.tipo))
        hoja.write(row_num, 1, empleado.numero)
        hoja.write(row_num, 2, empleado.apellido_paterno + ' ' + empleado.apellido_materno + ' ' + empleado.nombre)
        hoja.write(row_num, 3, str(empleado.departamento))
        hoja.write(row_num, 4, str(empleado.puesto))
        hoja.write(row_num, 5, empleado.dias_vacaciones)
        row_num += 1

    
    libro.save(response)

    return response

def filtro_empleados(tipo, departamento, puesto, nombre, plan):
    
    #Sin Filtro
    if departamento == None and puesto == None:
        empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = tipo.descripcion), Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), plan = plan)

    #Filtra x departamento
    if departamento != None and puesto == None:
        empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = tipo.descripcion), Q(departamento__descripcion__contains = departamento.descripcion),  Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), plan = plan)
        
    #Filtra x puesto
    if departamento == None and puesto != None:
        empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = tipo.descripcion), Q(puesto__descripcion__contains = puesto.descripcion),  Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), plan = plan)

    #Filtra x departamento Y puesto
    if departamento != None and puesto != None:
        empleados = Empleado.objects.filter(Q(tipo__descripcion__contains = tipo.descripcion), Q(departamento__descripcion__contains = departamento.descripcion), Q(puesto__descripcion__contains = puesto.descripcion),  Q(nombre__icontains = nombre) | Q(apellido_paterno__icontains = nombre) | Q(apellido_materno__icontains = nombre), plan = plan)
    
    return empleados.order_by('departamento')