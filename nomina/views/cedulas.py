from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def cedulas(request):
    if request.user.is_authenticated:
        context = { 'url_name': 'cedulas'}
        return render(request, 'nomina/menu/cedulas.html', context)
    else:
        return redirect('login')