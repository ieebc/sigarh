from django import forms
from django.contrib.auth.forms import AuthenticationForm
from nomina.models import *
from .class_select import class_form_select

class SigaLoginForm(AuthenticationForm):
    pass
    plan = forms.ModelChoiceField(label='Plan', queryset=Plan.objects.filter(clonado=True, eliminando=False),widget=forms.Select(attrs=class_form_select), initial=1)
