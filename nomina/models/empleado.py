from django.db import models
from django.db.models import Q
import datetime
from datetime import date
from calendar import monthrange

from .plan import Plan, DEFAULT_PLAN_ID
from .tipoempleado import TipoEmpleado
from .departamento import Departamento
from .puesto import Puesto
from .vacaciones import Vacaciones
from .rama import Rama
from .nivel import Nivel

from nomina.models import *

class Estructura(models.Model):
    tipo = models.ForeignKey(TipoEmpleado, on_delete=models.RESTRICT)
    rama = models.ForeignKey(Rama, on_delete=models.RESTRICT)
    nivel = models.ForeignKey(Nivel, on_delete=models.RESTRICT)
    puesto = models.ForeignKey(Puesto, on_delete=models.RESTRICT)
    clave = models.CharField(max_length=255)
    nivel_consecutivo = models.IntegerField()
    nivel_tabular = models.IntegerField()
    sueldo = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    def __str__(self):
        return '%s - %s' % (self.puesto, self.tipo)
    
    @property
    def empleados(self):
        return Empleado.objects.filter(plan = self.plan, puesto = self.puesto, tipo = self.tipo).count()

class Empleado(models.Model):
    plazas = models.IntegerField(default=1)
    tipo = models.ForeignKey(TipoEmpleado, on_delete=models.RESTRICT)
    numero = models.CharField(max_length=255)
    departamento = models.ForeignKey(Departamento, on_delete=models.RESTRICT)
    puesto = models.ForeignKey(Puesto, on_delete=models.RESTRICT)
    nombre = models.CharField(max_length=255)
    apellido_paterno = models.CharField(max_length=255, blank=True)
    apellido_materno = models.CharField(max_length=255, blank=True)
    vacaciones = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    excedente = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    spen = models.BooleanField(default=False)
    plan = models.ForeignKey(Plan, on_delete=models.RESTRICT, default=DEFAULT_PLAN_ID)

    class Meta:
        ordering = ['departamento__orden']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.periodos = self.periodo_set.all()
        self.fecha_ingreso = self.get_fecha_ingreso()
        self.sueldo = self.get_sueldo()

    def __str__(self):
        return self.nombre
    
    def nombre_completo(self):
        return self.apellido_paterno + ' ' + self.apellido_materno + ' ' + self.nombre

    def get_sueldo(self):
        if self.id:
            estructura = Estructura.objects.filter(tipo = self.tipo, puesto = self.puesto, plan = self.plan).first()
            if estructura:
                return estructura.sueldo     
        return 0
    
    def get_fecha_ingreso(self):
        if len(self.periodos) > 0:
            return self.periodos.first().fecha_ingreso
        return date.today()
    
    def es_incapacitado(self):
        return (self.puesto.descripcion in ['INCAPACITADO PERMANENTE "A"', 'INCAPACITADO PERMAMENTE "B"', 'INCAPACITADO PERMANENTE "C"'])

    def set_dias_vacaciones(self, anio):
        if self.tipo.es_confianza():
            self.dias_vacaciones = self.vacaciones
        else:
            self.dias_vacaciones = 20

    def set_periodos_dias(self):
        dias = 0
        if not self.tipo.es_confianza():
            for periodo in self.periodos:
                dias += periodo.dias

        self.periodos_dias = dias    

    def set_bono(self):
        self.recibe_bono = True
        puestos = ['CONSEJERO PRESIDENTE', 'CONSEJERO ELECTORAL']
        departamentos = ['CONSEJO GENERAL ELECTORAL']
        if (self.puesto.descripcion in puestos) and (self.departamento.descripcion in departamentos):
            self.recibe_bono = False
        
        puestos = ['ENLACE ADMINISTRATIVO', 'DELEGADO MUNICIPAL', 'DELEGADO DISTRITAL']
        if (('CONSEJO DISTRITAL' in self.departamento.descripcion) or (self.puesto.descripcion in puestos)) and (not self.tipo.es_confianza()):
            self.recibe_bono = False

    def set_transporte(self):
        self.recibe_transporte = False
        puestos = ['SECRETARIO FEDATARIO', 'DELEGADO DISTRITAL', 'DELEGADO MUNICIPAL']
        if (self.puesto.descripcion in puestos):
            self.recibe_transporte = True

    def set_gastos(self):
        self.recibe_gastos = False
        puestos = ['CAES', 'SUPERVISOR ELECTORAL']
        if (self.puesto.descripcion in puestos):
            self.recibe_gastos = True

    def mes_dias(self, anio, mes):
        primer_dia_mes = datetime.date(anio, mes, 1)
        ultimo_dia_mes = datetime.date(anio, mes, monthrange(anio, mes)[1])

        mes_dias = 0
        fecha_inicio = 0
        fecha_final = 0

        for periodo in self.periodos:
            #Determinar el mas grande de las dos fechas iniciales
            if periodo.fecha_ingreso > primer_dia_mes:
                fecha_inicio = periodo.fecha_ingreso
            else:
                fecha_inicio = primer_dia_mes

            #Determinar la menor de las dos fechas finales
            if periodo.fecha_baja < ultimo_dia_mes:
                fecha_final = periodo.fecha_baja
            else:
                fecha_final = ultimo_dia_mes
        
            dias_calculo = (fecha_final - fecha_inicio).days + 1

            if dias_calculo > 0:
                mes_dias += dias_calculo

        return mes_dias
    
    def mes_dias_baja(self, anio, mes):
        primer_dia_mes = datetime.date(anio, mes, 1)
        ultimo_dia_mes = datetime.date(anio, mes, monthrange(anio, mes)[1])

        mes_dias = 0

        periodos_empleado = self.periodos.filter(Q(fecha_baja__gte = primer_dia_mes), Q(fecha_baja__lte = ultimo_dia_mes))
        for periodo in periodos_empleado:
            dias_calculo = (periodo.fecha_baja - periodo.fecha_ingreso).days + 1

            if dias_calculo > 0:
                mes_dias += dias_calculo

        return mes_dias

    def set_meses_dias(self, anio):
        self.meses_dias = [0,0,0,0,0,0,0,0,0,0,0,0]
        self.meses_dias_baja = [0,0,0,0,0,0,0,0,0,0,0,0]

        for periodo in self.periodos:
            dia_ingreso = periodo.fecha_ingreso
            dia_baja = periodo.fecha_baja

            while dia_ingreso <= dia_baja:
                self.meses_dias[dia_ingreso.month - 1] += 1
                dia_ingreso = dia_ingreso + datetime.timedelta(days=1)

            self.meses_dias_baja[dia_baja.month - 1] = periodo.dias

    def set_catorcenas_dias(self, catorcenas):
        #Obtiene dias contratados por catorcena
        self.catorcenas_dias = [0] * len(catorcenas)
        i = 0

        for catorcena in catorcenas:
            dias_catorcena = self.get_dias_periodo_catorcena(catorcena)
            self.catorcenas_dias[i] = dias_catorcena
            i += 1
    
    def get_dias_periodo_catorcena(self, catorcena):
        dias_catorcena = 0

        for periodo in self.periodos:
            #Determinar el mas grande de las dos fechas iniciales
            if periodo.fecha_ingreso > catorcena.fecha_inicio:
                fecha_inicio = periodo.fecha_ingreso
            else:
                fecha_inicio = catorcena.fecha_inicio

            #Determinar la menor de las dos fechas finales
            if not periodo.fecha_baja:
                fecha_final = catorcena.fecha_final
            else:
                if periodo.fecha_baja < catorcena.fecha_final:
                    fecha_final = periodo.fecha_baja
                else:
                    fecha_final = catorcena.fecha_final
            
            dias_calculo = (fecha_final - fecha_inicio).days + 1
            if dias_calculo > 0:
                dias_catorcena += dias_calculo

        return dias_catorcena
        
    def set_data_emp(self, anio, catorcenas):
        self.set_dias_vacaciones(anio)
        self.set_bono()
        self.set_periodos_dias()
        self.set_catorcenas_dias(catorcenas)
        self.set_transporte()
        self.set_gastos()

        if not self.tipo.es_confianza():
            self.set_meses_dias(anio)


class Periodo(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
    fecha_ingreso = models.DateField()
    fecha_baja = models.DateField(null=True, blank=True)

    def __str__(self):
        return '%s al %s (Dias: %s)' % (self.fecha_ingreso, self.fecha_baja, self.dias)

    @property
    def dias(self):
        return (self.fecha_baja - self.fecha_ingreso).days + 1

    class Meta:
        ordering = ['fecha_ingreso']
