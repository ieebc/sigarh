$(document).ready(function(){
    var btn;
    
    $('#id_anio').on('change', function() {
        $("#proyeccion-form").submit()
    });
    $('#id_tipo').on('change', function() {
        $("#proyeccion-form").submit()
    });
    $('#id_departamento').on('change', function() {
        $("#proyeccion-form").submit()
    });
    $('#id_puesto').on('change', function() {
        $("#proyeccion-form").submit()
    });
    $('#id_detalle').on('change', function() {
        $("#proyeccion-form").submit()
    });
    $('#id_nombre').on('change', function() {
        $("#proyeccion-form").submit()
    });

    $('#id_nombre').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
            $("#proyeccion-form").submit()
            return false;  
         }
    });
    
    $('#exportar-proyeccion').click(function() {
        btn = $(this);
    })

    $('#proyeccion-form').on('submit', function(e) {
        $('#loaderModal').modal('show');
        if (typeof btn !== 'undefined'){
            console.log("testing")
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), 
                success: function(response)
                {
                    if (typeof btn !== 'undefined'){
                        $('#loaderModal').modal('hide')
                        btn = undefined
                    }
                }
            });
        }
    });
});