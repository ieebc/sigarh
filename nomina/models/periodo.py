from django.db import models
from .empleado import Empleado

class Periodo(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
    fecha_ingreso = models.DateField()
    fecha_baja = models.DateField(null=True, blank=True)

    def __str__(self):
        return '%s al %s (Dias: %s)' % (self.fecha_ingreso, self.fecha_baja, self.dias)

    @property
    def dias(self):
        return (self.fecha_baja - self.fecha_ingreso).days + 1

    class Meta:
        ordering = ['fecha_ingreso']
