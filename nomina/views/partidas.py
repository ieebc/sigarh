from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def partidas_list(request):    
    plan = request.session['plan']
    partidas = Partida.objects.filter(plan = plan).order_by('orden')
    orden = 1
    for partida in partidas:
        partida.orden = orden
        partida.save()
        orden += 1

    partidas = Partida.objects.filter(plan = plan).order_by('orden')
    context = { 'partidas': partidas, 'url_name': 'partidas'}
    return render(request, 'nomina/partidas/content.html', context)

@login_required
def partida_nuevo(request):
    plan = request.session['plan']
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = PartidaForm(request.POST)
        if form.is_valid():
            partida = form.save(commit=False)
            partida.user = current_user
            partida.orden = Partida.objects.filter(plan = plan).count() + 1
            partida.plan = Plan.objects.filter(id = plan).first()
            partida.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('partidas')
    else:
        form = PartidaForm()

    return render(request, 'nomina/partidas/modal-nuevo.html', {'form': form})

@login_required
def partida_editar(request, pk):
    partida = get_object_or_404(Partida, pk = pk)
    if request.method == 'POST':
        form = PartidaForm(request.POST, instance=partida)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de partida exitosa!')
            return redirect('partidas')
    else:
        form = PartidaForm(instance=partida)
    return render(request, 'nomina/partidas/modal-editar.html', {'form': form, 'partida': partida})

@login_required
def partida_eliminar(request, pk):
    partida = get_object_or_404(Partida, pk = pk)
    if request.is_ajax():
        context = { 'partida': partida }
        return render(request, 'nomina/partidas/modal-eliminar.html', context)
    else:
        partida.delete()
        messages.success(request, 'Eliminación de partida exitosa!')
        return redirect('partidas')

def partida_subir(request, pk):
    plan = request.session['plan']
    partida_sube = get_object_or_404(Partida, pk = pk)
    partida_baja = Partida.objects.filter(orden = partida_sube.orden - 1, plan = plan).first()
    if partida_baja:
        partida_sube.orden -= 1
        partida_baja.orden += 1

        partida_sube.save()
        partida_baja.save()

    return redirect('partidas')

def partida_bajar(request, pk):
    plan = request.session['plan']
    partida_baja = get_object_or_404(Partida, pk = pk)
    partida_sube = Partida.objects.filter(orden = partida_baja.orden + 1, plan = plan).first()

    if partida_sube:
        partida_sube.orden -= 1
        partida_baja.orden += 1

        partida_sube.save()
        partida_baja.save()
        
    return redirect('partidas')