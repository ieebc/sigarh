from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, Submit
from crispy_forms.bootstrap import FormActions
from nomina.models import *
from .class_select import class_form_select_sm
from .inputs import DateInput

class CatorcenasSyncForm(forms.Form):    
    fecha_inicial = forms.DateField(required=True, widget=DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}), label='Fecha de inicio primera catorcena')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Field('fecha_inicial', wrapper_class="col-2"),
            FormActions(
                Submit('save', 'Generar Catorcenas'),
            ),
        )
