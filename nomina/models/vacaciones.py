from django.db import models

class Vacaciones(models.Model):
    dias = models.IntegerField(default=0)
    anio_limite_inferior = models.IntegerField(default=0)
    anio_limite_superior = models.IntegerField(default=0)

    def __str__(self):
        return '%s a %s años' % (self.anio_limite_inferior, self.anio_limite_superior)
