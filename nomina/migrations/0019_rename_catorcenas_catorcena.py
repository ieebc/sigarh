# Generated by Django 3.2.5 on 2021-10-27 19:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nomina', '0018_auto_20211027_1141'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Catorcenas',
            new_name='Catorcena',
        ),
    ]
