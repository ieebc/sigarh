# Generated by Django 3.2.5 on 2021-10-26 20:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('nomina', '0012_isr'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subsidio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('limite_inferior', models.FloatField(default=0)),
                ('limite_superior', models.FloatField(default=0)),
                ('cuota_fija', models.FloatField(default=0)),
                ('plan', models.ForeignKey(default=1, on_delete=django.db.models.deletion.RESTRICT, to='nomina.plan')),
            ],
            options={
                'ordering': ['limite_inferior'],
            },
        ),
    ]
