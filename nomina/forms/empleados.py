import re
from django import forms
from django.template.loader import render_to_string
from django.forms.models import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Row, Field, Fieldset, LayoutObject, TEMPLATE_PACK 
from crispy_forms.bootstrap import PrependedAppendedText
from nomina.models import *
from .class_select import class_form_select_sm
from .inputs import DateInput

class Formset(LayoutObject):
    template = 'nomina/empleados/formset.html'

    def __init__(self, formset_name_in_context, template=None):
        self.formset_name_in_context = formset_name_in_context
        self.fields = []
        if template:
            self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        formset = context[self.formset_name_in_context]
        return render_to_string(self.template, {'formset': formset})

class PeriodoForm(forms.ModelForm):    
    
    class Meta:
        model = Periodo
        exclude = ()
        widgets = {
            'fecha_ingreso': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'fecha_baja': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        formtag_prefix = re.sub('-[0-9]+$', '', kwargs.get('prefix', ''))

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Field('fecha_ingreso', wrapper_class="col-5"),
                Field('fecha_baja', wrapper_class="col-5"),
                Field('DELETE'),
                css_class='formset_row-{}'.format(formtag_prefix),
            )
        )

PeriodoFormset = inlineformset_factory(
    Empleado, Periodo, form=PeriodoForm, fields=['fecha_ingreso', 'fecha_baja'], 
    extra=1, can_delete=True, min_num=1, max_num=5
)

class EmpleadoForm(forms.ModelForm):  
    sueldo = forms.DecimalField(label='Sueldo',widget=forms.NumberInput(attrs=class_form_select_sm), initial=0)

    class Meta:
        model = Empleado
        fields = ['plazas','tipo', 'departamento', 'puesto', 'numero', 'nombre', 'apellido_paterno', 'apellido_materno', 'vacaciones', 'excedente', 'spen', ]
        labels = {
            'numero': 'No.',
            'nombre': 'Nombres',
            'apellido_paterno': 'Paterno',
            'apellido_materno': 'Materno',
            'spen': 'SPEN',
            'plazas': 'Plazas'
        }

        widgets = {
            'plazas': forms.NumberInput(attrs=class_form_select_sm),
            'tipo': forms.Select(attrs=class_form_select_sm),
            'departamento': forms.Select(attrs=class_form_select_sm),
            'puesto': forms.Select(attrs=class_form_select_sm),
            'numero': forms.TextInput(attrs=class_form_select_sm),
            'nombre': forms.TextInput(attrs=class_form_select_sm),
            'apellido_paterno': forms.TextInput(attrs=class_form_select_sm),
            'apellido_materno': forms.TextInput(attrs=class_form_select_sm),
            'vacaciones': forms.NumberInput(attrs=class_form_select_sm),
            'excedente': forms.NumberInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
        super(EmpleadoForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('plazas', wrapper_class="col-2"),
                Field('tipo', wrapper_class="col"),
                Field('numero', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('departamento', wrapper_class="col-4"),
                Field('puesto', wrapper_class="col-4"),
                Field('sueldo', wrapper_class="col-4", readonly=True),
                css_class='row'),
            Div(
                Field('nombre', wrapper_class="col"),
                Field('apellido_paterno', wrapper_class="col"),
                Field('apellido_materno', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('vacaciones', wrapper_class="col"),
                Field('excedente', wrapper_class="col"),
                css_class='row'),
            Div(
                Field('spen', wrapper_class="col"),
                css_class='row'),
            Div(
                Fieldset('Periodos de Contratación',
                    Formset('periodos'))
            )
        )

class EmpleadoFiltro(forms.Form):
    tipo = forms.ModelChoiceField(label='Tipo', queryset=TipoEmpleado.objects.all(),widget=forms.Select(attrs=class_form_select_sm), initial=0)
    departamento = forms.ModelChoiceField(label='Departamento', queryset=None,widget=forms.Select(attrs=class_form_select_sm), empty_label="<TODOS>", required=False)
    puesto = forms.ModelChoiceField(label='Puesto', queryset=None,widget=forms.Select(attrs=class_form_select_sm), empty_label="<TODOS>", required=False)
    nombre = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def __init__(self, plan, *args, **kwargs):
        super().__init__(*args, **kwargs)        
        self.fields['departamento'].queryset = Departamento.objects.filter(plan = plan)
        self.fields['puesto'].queryset = Puesto.objects.filter(plan = plan)

        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('tipo', wrapper_class="col", id="id_tipo_filtro"),
                Field('departamento', wrapper_class="col" ),
            css_class='row'),

            Div(
                Field('puesto', wrapper_class="col", id="id_puesto_filtro"),
                Field('nombre', wrapper_class="col"),
            css_class='row'),
        )
