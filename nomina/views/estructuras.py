from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from nomina.models import *
from nomina.forms import *

@login_required
def estructuras_list(request):    
    tipo = None
    puesto = None
    plan = request.session['plan']
    if request.method == 'POST':
        form = EstructuraFiltro(plan, request.POST)
        if form.is_valid():
            tipo = form.cleaned_data['tipo']
            puesto = form.cleaned_data['puesto']
    else:
        form = EstructuraFiltro(plan)

    estructuras = filtro_estructuras(tipo, puesto, plan)
    context = { 'estructuras': estructuras, 'url_name': 'estructuras', 'form': form}
    return render(request, 'nomina/estructuras/content.html', context)

@login_required
def estructura_nuevo(request):
    plan = request.session['plan']
    if request.method == 'POST':
        current_user = get_object_or_404(User, pk=request.user.pk)
        form = EstructuraForm(plan, request.POST)
        if form.is_valid():
            estructura = form.save(commit=False)
            estructura.user = current_user
            estructura.plan = Plan.objects.filter(id = plan).first()
            estructura.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('estructuras')
    else:
        form = EstructuraForm(plan)
    return render(request, 'nomina/estructuras/modal-nuevo.html', {'form': form})

@login_required
def estructura_editar(request, pk):
    plan = request.session['plan']
    estructura = get_object_or_404(Estructura, pk = pk)
    if request.method == 'POST':
        form = EstructuraForm(plan, request.POST, instance=estructura)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de estructura exitosa!')
            return redirect('estructuras')
    else:
        form = EstructuraForm(plan, instance=estructura)

    return render(request, 'nomina/estructuras/modal-editar.html', {'form': form, 'estructura': estructura})

@login_required
def estructura_eliminar(request, pk):
    estructura = get_object_or_404(Estructura, pk = pk)
    if request.is_ajax():
        context = { 'estructura': estructura }
        return render(request, 'nomina/estructuras/modal-eliminar.html', context)
    else:
        estructura.delete()
        messages.success(request, 'Eliminación de estructura exitosa!')
        return redirect('estructuras')

@login_required
def estructura_sueldos_incr(request):
    plan = request.session['plan']
    if request.method == 'POST':
        form = EstructuraSueldosForm(request.POST)
        if form.is_valid():
            porcentaje = form.cleaned_data['porcentaje']
            porcentaje = porcentaje / 100

            estructuras = Estructura.objects.filter(plan = plan)
            for estructura in estructuras:
                estructura.sueldo = estructura.sueldo + (estructura.sueldo * porcentaje)
                estructura.save()

            messages.success(request, 'Sueldos de estructura modificados exitosamente!')
            return redirect('estructuras')
    else:
        form = EstructuraSueldosForm()

    return render(request, 'nomina/estructuras/modal-porcentaje.html', {'form': form })

def filtro_estructuras(tipo, puesto, plan):
    if puesto == None and tipo == None:
        estructuras = Estructura.objects.filter(plan = plan)
        
    if puesto != None and tipo == None:
        estructuras = Estructura.objects.filter(Q(puesto__descripcion__contains = puesto.descripcion), plan = plan)

    if puesto == None and tipo != None:
        estructuras = Estructura.objects.filter(Q(tipo__descripcion__contains = tipo.descripcion), plan = plan)
    
    if puesto != None and tipo != None:
        estructuras = Estructura.objects.filter(Q(tipo__descripcion__contains = tipo.descripcion), Q(puesto__descripcion__contains = puesto.descripcion), plan = plan)

    return estructuras