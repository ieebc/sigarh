
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from nomina.models import *
from nomina.forms import *

@login_required
def home(request):
    if request.user.is_authenticated:
        context = { 'url_name': 'home'}
        return render(request, 'nomina/menu/home.html', context)
    else:
        return redirect('login')