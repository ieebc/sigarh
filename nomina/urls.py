from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import TemplateView

from nomina import views
from nomina.views import *

urlpatterns = [
    #Home
    path('', views.home, name='home'),
    path('proyeccion/', views.proyeccion_list, name='proyeccion'),

    #FONACOT
    path('fonacots/', views.fonacot_list, name='fonacots'),
    path('fonacot/', views.fonacot_nuevo, name='fonacot-nuevo'),
    path('fonacot/<int:pk>/', views.fonacot_editar, name='fonacot-editar'),
    path('fonacot/<int:pk>/eliminar/', views.fonacot_eliminar, name='fonacot-eliminar'),
    path('fonacot/exportar-txt/', views.fonacot_exportar_txt, name='fonacot-exportar-txt'),
    path('fonacot/exportar-csv/', views.fonacot_exportar_csv, name='fonacot-exportar-csv'),

    #Empleados
    path('empleados/', views.empleados_list, name='empleados'),
    path('empleado/', login_required(EmpleadoCreateView.as_view()), name='empleado-nuevo'),
    path('empleado/<int:pk>/', login_required(EmpleadoUpdateView.as_view()), name='empleado-editar'),
    path('empleado/<int:pk>/eliminar/', views.empleado_eliminar, name='empleado-eliminar'),
    path('empleados/eliminar/', views.empleados_eliminar, name='empleados-eliminar'),
    path('empleados/reporte/', views.empleados_reporte, name='empleados-reporte'),
    path('empleados/sueldo/', views.sueldo, name='sueldo'),

    #Departamentos
    path('departamentos/', views.departamentos_list, name='departamentos'),
    path('departamento/', views.departamento_nuevo, name='departamento-nuevo'),
    path('departamento/<int:pk>/', views.departamento_editar, name='departamento-editar'),
    path('departamento/<int:pk>/eliminar/', views.departamento_eliminar, name='departamento-eliminar'),
    path('departamento/<int:pk>/subir/', views.departamento_subir, name='departamento-subir'),
    path('departamento/<int:pk>/bajar/', views.departamento_bajar, name='departamento-bajar'),

    #Puestos
    path('puestos/', views.puestos_list, name='puestos'),
    path('puesto/', views.puesto_nuevo, name='puesto-nuevo'),
    path('puesto/<int:pk>/', views.puesto_editar, name='puesto-editar'),
    path('puesto/<int:pk>/eliminar/', views.puesto_eliminar, name='puesto-eliminar'),

    #Catorcenas
    path('catorcenas/', views.catorcenas_list, name='catorcenas'),
    path('catorcenas/sincronizar/', views.catorcena_sincronizar, name='catorcenas-sincronizar'),

    #Partidas
    path('partidas/', views.partidas_list, name='partidas'),
    path('partida/', views.partida_nuevo, name='partida-nuevo'),
    path('partida/<int:pk>/', views.partida_editar, name='partida-editar'),
    path('partida/<int:pk>/eliminar/', views.partida_eliminar, name='partida-eliminar'),
    path('partida/<int:pk>/subir/', views.partida_subir, name='partida-subir'),
    path('partida/<int:pk>/bajar/', views.partida_bajar, name='partida-bajar'),

    #Estructuras
    path('estructuras/', views.estructuras_list, name='estructuras'),
    path('estructuras/sueldos/', views.estructura_sueldos_incr, name='estructuras-sueldos'),
    path('estructura/', views.estructura_nuevo, name='estructura-nuevo'),
    path('estructura/<int:pk>/', views.estructura_editar, name='estructura-editar'),
    path('estructura/<int:pk>/eliminar/', views.estructura_eliminar, name='estructura-eliminar'),

    #ISR
    path('isrs/', views.isrs_list, name='isrs'),
    path('isr/', views.isr_nuevo, name='isr-nuevo'),
    path('isr/<int:pk>/', views.isr_editar, name='isr-editar'),
    path('isr/<int:pk>/eliminar/', views.isr_eliminar, name='isr-eliminar'),

    #Subsidio
    path('subsidios/', views.subsidios_list, name='subsidios'),
    path('subsidio/', views.subsidio_nuevo, name='subsidio-nuevo'),
    path('subsidio/<int:pk>/', views.subsidio_editar, name='subsidio-editar'),
    path('subsidio/<int:pk>/eliminar/', views.subsidio_eliminar, name='subsidio-eliminar'),

    #Planes
    path('planes/', views.planes_list, name='planes'),
    path('plan/<int:pk>/', views.plan_editar, name='plan-editar'),
    path('plan/<int:pk>/cargar/', views.plan_cargar, name='plan-cargar'),
    path('plan/<int:pk>/clonar/', views.plan_clonar, name='plan-clonar'),
    path('plan/<int:pk>/eliminar/', views.plan_eliminar, name='plan-eliminar'),
    
    #AUTH
    path('login/', SigaLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(template_name='nomina/logout.html'), name='logout'),

    #SCRIPTS
    path('scripts/', views.scripts, name='scripts'),
    path('scripts/departamentos/', views.scripts_departamentos, name='scripts-departamentos'),
    path('scripts/puestos/', views.scripts_puestos, name='scripts-puestos'),
    path('scripts/estructuras/', views.scripts_estructuras, name='scripts-estructuras'),
    path('scripts/empleados/', views.scripts_empleados, name='scripts-empleados'),
    path('scripts/partidas/', views.scripts_partidas, name='scripts-partidas'),
    path('scripts/departamentos-plan/', views.scripts_departamentos_plan, name='scripts-departamentos-plan'),
    path('scripts/puestos-plan/', views.scripts_puestos_plan, name='scripts-puestos-plan'),
    path('scripts/plan-sincronizar/', views.scripts_plan_sincronizar, name='scripts-plan-sincronizar'),
    path('scripts/plan-sueldos/', views.scripts_plan_sueldos, name='scripts-plan-sueldos'),
    path('scripts/plan-isr/', views.scripts_plan_isr, name='scripts-plan-isr'),
    path('scripts/asimilables/', views.scripts_asimilables, name='scripts-asimilables'),
    path('scripts/consejeros/', views.scripts_consejeros, name='scripts-consejeros'),
    path('scripts/fonacot/', views.scripts_fonacot, name='scripts-fonacot'),
    path('scripts/fonacot-csv/', views.scripts_fonacot_csv, name='scripts-fonacot-csv'),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)